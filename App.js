import React, { Component } from "react";
import { StatusBar, View } from "react-native";
import { Provider } from "react-redux";
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import store from "./src/redux_store/store";
import Splash from "./src/screen/Splash";
import DisplayScreen from "./src/screen/DisplayScreen";
import FilterScreen from "./src/screen/FilterScreen";
import Dashboard from "./src/screen/Dashboard";
import ImageFilterScreen from "./src/screen/ImageFilterScreen";
import OpenImageScreen from "./src/screen/OpenImageScreen";
import ImageSwitchingScreen from "./src/screen/ImageSwitchingScreen";
import OpenSettingScreen from "./src/screen/OpenSettingScreen";
import AnimatedScreen from "./src/screen/AnimatedScreen";
import OpenIntentScreen from "./src/screen/OpenIntentScreen";
import AutoComplete from "./src/screen/AutoComplete";
import ApiFetchingScreen from "./src/screen/ApiFetchingScreen";
import InfoComponentScreen from "./src/screen/InfoComponentScreen";
import TextInputScreen from "./src/screen/TextInputScreen";
import DisplayUserPost from "./src/screen/DisplayUserPost";
import OpenListComponentScreen from "./src/screen/OpenListComponentScreen";
import SignupContainer from "./src/screen/Signup/SignupContainer";
import SignupUIScreen from "./src/screen/Signup/SignupUIScreen";
import LoginUIScreen from "./src/screen/Login/LoginUIScreen";
import LoginContainer from "./src/screen/Login/LoginContainer";
import PdfScreen from "./src/screen/PdfScreen";
import AnimatedScreen1 from "./src/screen/AnimatedScreen1";
import AnimatedScreen2 from "./src/screen/AnimatedScreen2";
import FlatlistScreen from "./src/screen/FlatlistScreen";
import FormDataApiScreen from "./src/screen/FormDataApiScreen";
import DemoUI from "./src/screen/DemoUI";
import MemoScreen from "./src/screen/MemoScreen";
import RatingScreen from "./src/screen/RatingScreen";
import AddItems from "./src/screen/AddItems";
import IAPScreen from "./src/screen/IAPScreen";
import IAPScreen1 from "./src/screen/IAPScreen 1";
import CreatePdfScreen from "./src/screen/CreatePdfScreen";




const Stack = createStackNavigator()

class App extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <Provider store={store} >
        <View style={{ flex: 1 }}>
          <StatusBar backgroundColor={'#37474f'}
            barStyle={"light-content"}
          />
          <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
              <Stack.Screen name="Splash" component={Splash} />
              <Stack.Screen name="DisplayScreen" component={DisplayScreen} />
              <Stack.Screen name="FilterScreen" component={FilterScreen} />
              <Stack.Screen name="Dashboard" component={Dashboard} />
              <Stack.Screen name="OpenListComponentScreen" component={OpenListComponentScreen} />
              <Stack.Screen name="ImageFilterScreen" component={ImageFilterScreen} />
              <Stack.Screen name="TextInputScreen" component={TextInputScreen} />
              <Stack.Screen name="DisplayUserPost" component={DisplayUserPost} />
              <Stack.Screen name="OpenImageScreen" component={OpenImageScreen} />
              <Stack.Screen name="OpenSettingScreen" component={OpenSettingScreen} />
              <Stack.Screen name="ImageSwitchingScreen" component={ImageSwitchingScreen} />
              <Stack.Screen name="AnimatedScreen" component={AnimatedScreen} />
              <Stack.Screen name="OpenIntentScreen" component={OpenIntentScreen} />
              <Stack.Screen name='AutoComplete' component={AutoComplete} />
              <Stack.Screen name='ApiFetchingScreen' component={ApiFetchingScreen} />
              <Stack.Screen name='InfoComponentScreen' component={InfoComponentScreen} />
              <Stack.Screen name='LoginUIScreen' component={LoginUIScreen} />
              <Stack.Screen name='LoginContainer' component={LoginContainer} />
              <Stack.Screen name='SignupContainer' component={SignupContainer} />
              <Stack.Screen name='SignupUIScreen' component={SignupUIScreen} />
              <Stack.Screen name='PdfScreen' component={PdfScreen} />
              <Stack.Screen name='AnimatedScreen1' component={AnimatedScreen1} />
              <Stack.Screen name='AnimatedScreen2' component={AnimatedScreen2} />
              <Stack.Screen name='FlatlistScreen' component={FlatlistScreen} />
              <Stack.Screen name='FormDataApiScreen' component={FormDataApiScreen} />
              <Stack.Screen name='DemoUI' component={DemoUI} />
              <Stack.Screen name='MemoScreen' component={MemoScreen} />
              <Stack.Screen name='RatingScreen' component={RatingScreen} />
              <Stack.Screen name='AddItems' component={AddItems} />
              <Stack.Screen name='CreatePdfScreen' component={CreatePdfScreen} />
              <Stack.Screen name='IAPScreen' component={IAPScreen} />
              <Stack.Screen name='IAPScreen1' component={IAPScreen1} />







            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    )
  }
}

export default App;