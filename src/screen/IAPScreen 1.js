import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, TextInput, Alert, Image } from 'react-native';
import AnimatedComponent from '../component/AnimatedComponent';
import UseEffectFetchAPI from '../component/UseEffectFetchAPI';
class IAPScreen1 extends Component {
  
    render() {
      return (
        <View style={styles.container}>
          <Text>***********************</Text>

          <AnimatedComponent />

          <UseEffectFetchAPI />
        </View>
      );
    }
  }
  const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
    }
})
  export default IAPScreen1;