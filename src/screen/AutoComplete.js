import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, TextInput } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setIsAutocomplete, setSearchState, setState, setTemp } from '../redux_store/actions/indexActions';

class AutoComplete extends Component {
    constructor(props) {
        super(props);
        this.props.setTemp(this.props.state_list)
    }

    _renderItem = (item) => {
        return (
            <View style={{
                padding: 15,
                width: 353,
            }}>
                <TouchableOpacity
                    onPress={() => {
                        // alert(item.item.name)
                        this.props.setState(item.item.name);
                        this.props.setIsAutocomplete(false)

                    }}
                >

                    <Text>
                        {item.item.name}
                    </Text>
                </TouchableOpacity>


            </View>
        )
    };


    render() {
        return (
            <View style={{
                flex: 1,
            }}>

                <TextInput
                    placeholder="State..."
                    value={this.props.states}
                    placeholderTextColor='white'
                    style={{
                        backgroundColor: "gray",
                        marginHorizontal: 10,
                        marginTop: 10,
                        borderRadius: 10,
                        fontSize: 20,
                        paddingHorizontal: 10
                    }}
                    onChangeText={(txt) => {
                        // this.props.setState(txt);
                        this.props.setIsAutocomplete(true)
                    }}
                />

                {
                    this.props.is_autocomplete ?
                        <View style={{
                            backgroundColor: "pink",
                            borderRadius: 10,
                            paddingHorizontal: 10,
                            marginHorizontal: 10,
                            height: 200,
                            marginTop: 55,
                            position: "absolute",
                            elevation: 10,
                        }}>

                            <FlatList
                                data={this.props.state_list}
                                renderItem={this._renderItem}
                            />

                        </View>
                        : null
                }
                {/* <TextInput placeholder="State..."
                    placeholderTextColor='white'
                    style={{
                        backgroundColor: "gray",
                        marginHorizontal: 10,
                        marginTop: 10,
                        borderRadius: 10,
                        fontSize: 20,
                        paddingHorizontal: 10
                    }} /> */}

            </View>
        );
    }
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        borderStartColor: "#FF1744",
        // backgroundColor:'white'
    },

});
const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        state_list: getState.state_list,
        states: getState.states,
        is_autocomplete: getState.is_autocomplete
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setAxiosList: (list) => setAxiosList(list)
            setState: (state) => setState(state),
            setIsAutocomplete: (is_autocomplete) => setIsAutocomplete(is_autocomplete),
            setSearchState: (name) => setSearchState(name),
            setTemp: (temp) => setTemp(temp)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(AutoComplete);
