import React, { Component } from 'react'
import { ScrollView, StyleSheet, View } from 'react-native';
import Option from '../component/Option';
class Dashboard extends Component {
    render() {
        return (
            <View style={styles.container} >
                <ScrollView
                >
                    <View style={styles.rowStyle} >
                        <Option
                            option_name="FilterScreen"
                            onPress={() => this.props.navigation.navigate("FilterScreen")}
                        />

                        <Option
                            option_name="Snackbar"
                            onPress={() => this.props.navigation.navigate("DisplayScreen")}
                        />

                        <Option
                            option_name="ImageSwitchingScreen"
                            onPress={() => this.props.navigation.navigate("ImageSwitchingScreen")}
                        />

                    </View>
                    {/* //////////////////////////////////////////////////////////////////////////////////////////////////////// */}
                    <View style={styles.rowStyle} >
                        <Option
                            option_name="Auto Complete"
                            onPress={() => this.props.navigation.navigate("AutoComplete")}
                        />

                        <Option
                            option_name="Api Fetching"
                            onPress={() => this.props.navigation.navigate("ApiFetchingScreen")}
                        />

                        <Option
                            option_name="Image Filter Screen"
                            onPress={() => this.props.navigation.navigate("ImageFilterScreen")}
                        />


                    </View>

                    {/* ///////////////////////////////////////////////////////// */}
                    <View style={styles.rowStyle} >
                        <Option
                            option_name="Display User"
                            onPress={() => this.props.navigation.navigate("DisplayUserPost")}
                        />

                        <Option
                            option_name="TextInput Screen"
                            onPress={() => this.props.navigation.navigate("TextInputScreen")}
                        />

                        <Option
                            option_name="Open Setting n Url with Linking"
                            onPress={() => this.props.navigation.navigate("OpenSettingScreen")}
                        />

                        <Option
                            option_name="Open Setting n Url with Linking"
                            onPress={() => this.props.navigation.navigate("LoginContainer")}
                        />

                    </View>


                    {/* //////////////////////////////////////////////////////////////// */}

                    <View style={styles.rowStyle} >


                        <Option
                            option_name="Login Signup"
                            onPress={() => this.props.navigation.navigate("LoginContainer")}
                        />
                        {/* <Option
                            option_name="Hooks"
                            onPress={() => this.props.navigation.navigate("HooksConceptScreen")}
                        /> */}
                        <Option
                            option_name="Pdf Screen"
                            onPress={() => this.props.navigation.navigate("PdfScreen")}
                        />
                        <Option
                            option_name="Animated Screen1"
                            onPress={() => this.props.navigation.navigate("AnimatedScreen1")}
                        />


                    </View>

                    <View style={styles.rowStyle} >
                        <Option
                            option_name="Animated Screen 2"
                            onPress={() => this.props.navigation.navigate("AnimatedScreen2")}
                        />
                        <Option
                            option_name="Flatlist Screen"
                            onPress={() => this.props.navigation.navigate("FlatlistScreen")}
                        />
                        <Option
                            option_name="FormDataApi Screen"
                            onPress={() => this.props.navigation.navigate("FormDataApiScreen")}
                        />
                    </View>

                    <View style={styles.rowStyle} >
                        <Option
                            option_name="Demo UI"
                            onPress={() => this.props.navigation.navigate("DemoUI")}
                        />
                        <Option
                            option_name="Memo Screen"
                            onPress={() => this.props.navigation.navigate("MemoScreen")}
                        />
                        <Option
                            option_name="Rating Screen"
                            onPress={() => this.props.navigation.navigate("RatingScreen")}
                        />
                    </View>

                    <View style={styles.rowStyle} >
                       
                        <Option
                            option_name="Create Pdf Screen"
                            onPress={() => this.props.navigation.navigate("CreatePdfScreen")}
                        />
                        <Option
                            option_name="IAP Screen"
                            onPress={() => this.props.navigation.navigate("IAPScreen")}
                        />

                    </View>

                    <View style={styles.rowStyle} >
                        <Option
                            option_name="Animated"
                            onPress={() => this.props.navigation.navigate("IAPScreen1")}
                        />
                        <Option
                            option_name="PdfScreen"
                            onPress={() => this.props.navigation.navigate("PdfScreen")}
                        />


                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    rowStyle: {
        flexDirection: "row",
        elevation: 12,

    }
})

export default Dashboard;