import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import IntentComponent from '../component/IntentComponent';


class OpenIntentScreen extends Component {

//onClick() {
//        Share.share({
//            message: 'BAM: we\'re helping your business with awesome React Native apps',
//            url: 'http://bam.tech',
//            title: 'Wow, did you see that?'
//        }, {
//            // Android only:
//            dialogTitle: 'Share BAM goodness',
//            // iOS only:
//            excludedActivityTypes: [
//                'com.apple.UIKit.activity.PostToTwitter'
//            ]
//        })
//    }

    render() {
        return (

            <View style={styles.container}>
                {/* <TouchableOpacity
                    style={{
                        height: 50,
                        width: 300,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'green',
                        elevation: 9,
                        borderRadius: 3

                    }}
                    onPress={() => this.handlePress()} >
                    <Text style={{ color: 'white', fontSize: 16, fontWeight: '600' }}>{'OPEN URL'} </Text>
                </TouchableOpacity> */}

               <IntentComponent />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(OpenIntentScreen);
