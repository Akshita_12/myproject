import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native';
import UserList_Row from '../screen_row.js/UserList_Row';
import { setUserDetails, setUserImage } from '../redux_store/actions/indexActions';

class DisplayUserPost extends Component {


  _onClick = (list) => {
    this.props.setUserImage(list.pic),
      this.props.setUserDetails(list.name + '\n'
        + list.id + '\n'
        + list.likes + '\n'
        + list.message + '\n'
        + list.hashTag),
      this.props.navigation.navigate('OpenListComponentScreen')
  }


  renderItem = (dataItem) => {
    return (
      <UserList_Row
        dataRow={dataItem.item}
        onClick={this._onClick}

      />
    )
  }




  render() {

    return (

      <View style={styles.container}>

          <FlatList
            data={this.props.user_post}
            renderItem={this.renderItem}
            keyExtractor ={(item,index)=>'key'+index}
          />

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center'
  },
  txtStyle: {
    fontSize: 28,
    color: 'blue',
    fontFamily: 'serif'
  },

});

const mapStateToProps = (state) => {
  let getState = state.indexReducer;

  return {
    user_post: getState.user_post
  };
};

const mapDsipatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setUserImage: (img) => setUserImage(img),
      setUserDetails: (details) => setUserDetails(details)
    },

    dispatch,
  );
};
export default connect(mapStateToProps, mapDsipatchToProps)(DisplayUserPost);