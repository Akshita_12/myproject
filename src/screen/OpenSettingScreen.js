import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Constants from '../common/Constants';

class OpenSettingScreen extends Component {

    handlePress = () => {
        Linking.openSettings();
    };

    openUrl = (url) => {
        Linking.canOpenURL(url).then(supported => {
            supported && Linking.openURL(url);
        }, (err) => console.log(err));
    }

    rateApp =()=> {
        const rate_url = Platform.OS === 'ios' ? Constants.IOS_RATE_URL : Constants.ANDROID_RATE_URL;
        Linking.canOpenURL(rate_url).then(supported => {
            supported && Linking.openURL(rate_url);
        }, (err) => console.log(err));
    }

    render() {
        return (

            <View style={styles.container}>
                <TouchableOpacity
                    style={{
                        height: 50,
                        width: 300,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'green',
                        elevation: 9,
                        borderRadius: 3

                    }}
                    onPress={() => this.handlePress()} >
                    <Text style={{ color: 'white', fontSize: 16, fontWeight: '600' }}>{'OPEN SETTINGS'} </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{
                        height: 50,
                        width: 300,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'green',
                        elevation: 9,
                        borderRadius: 3,
                        marginTop: 15

                    }}
                    onPress={() => this.rateApp()} >
                    <Text style={{ color: 'white', fontSize: 16, fontWeight: '600' }}>{'RATE APP'} </Text>
                </TouchableOpacity>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(OpenSettingScreen);
