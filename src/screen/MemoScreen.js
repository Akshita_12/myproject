import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header1 from '../component/Header1';
import { MemoizedMovie, Movie } from '../component/MemoComponent';

class MemoScreen extends Component {
    // _onGoBack = () => {
    //     this.props.navigation.goBack()
    // }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>

                    {/* <Header1
                        onGoBack={() => this._onGoBack()}
                        isCross={true}
                        barStyle='dark-content'
                        tintColor={'white'}
                    /> */}

                    <MemoizedMovie
                        title="Heat"
                        releaseDate="December 15, 1995"
                    />

                    <MemoizedMovie
                        title="Heat"
                        releaseDate="December 15, 1995"
                    />

                    <Image
                        style={{ height: 30, width: 30 }}
                        source={require('../assets/drop.png')}
                    />
                </View>



            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    txtStyle: {
        fontSize: 23,
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontWeight: 'bold'
    },
    imgStyle: {
        height: 300,
        width: 300,
        alignSelf: 'center'
    }

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(MemoScreen);
