import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserList_Row from '../screen_row.js/UserList_Row';
import Pagination,{Icon,Dot} from 'react-native-pagination';//{Icon,Dot} also available


class PaginationScreen extends Component {
    renderItem = (dataItem) => {
        return (
            <UserList_Row
                dataRow={dataItem.item}
                onClick={this._onClick}

            />
        )
    }
    render() {
        return (

            <View style={styles.container}>

                <FlatList
                    data={this.props.user_post}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => 'key' + index}
                />

                <Pagination
                    // dotThemeLight //<--use with backgroundColor:"grey"
                    // listRef={this.refs}//to allow React Native Pagination to scroll to item when clicked  (so add "ref={r=>this.refs=r}" to your list)
                    // paginationVisibleItems={this.state.viewableItems}//needs to track what the user sees
                    paginationItems={this.props.user_post}//pass the same list as data
                    paginationItemPadSize={3} //num of items to pad above and below your visable items
                />


            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        user_post: getState.user_post

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(PaginationScreen);
