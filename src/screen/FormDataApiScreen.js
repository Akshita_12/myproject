import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, Dimensions, Alert, ActivityIndicator } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';
import { hitUpdateProfileImageApi, setProfile } from '../redux_store/actions/indexActions';
import { debugLog } from '../common/Constants';

class FormDataApiScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }



    openPickerGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            isCamera: true,
        }).then((image) => {
            this.props.setProfile(image);
        });
    };

    openPickerCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 300,
            cropping: true,
        }).then((image) => {
            this.props.setProfile(image);
        });
    };

    showAlert = () => {
        Alert.alert(
            'Upload Profile Picture',
            'Please choose Camera or Gallery',
            [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            {
                text: 'Camera',
                onPress: () => {
                    this.openPickerCamera();
                },
            },
            {
                text: 'Gallery',
                onPress: () => {
                    this.openPickerGallery();
                },
            },
            ],
            {
                cancelable: false,
            },
        );
    }

    _startLoading = () => {
        this.setState({ loading: true })
    }

    _onClickSaveChanges = () => {
        this._startLoading()
        if (
            this.props.user_id == ''
        ) {
            alert('User ID is not found!');
            return;
        }

        let data = new FormData();
        // debugLog(image)
        if (this.props.set_profile != "") {
            let imageData = {
                uri: this.props.set_profile.path,
                type: this.props.set_profile.mime,
                name: 'pic.jpg'
            };
            // debugLog(imageData);
            data.append('profile_pic', imageData);
        }
        else {
            return;
        }

        data.append('user_id', 52);

        debugLog(data)

        this.props.hitUpdateProfileImageApi(data).then((response) => {
            let _response = response.workout;
            if (_response.status == 1) {
                this.setState({ loading: false })
                alert("Profile updated")
                console.log(_response);

            } else {
                alert("Something went wrong")
            }
        });
    };


    render() {
        return (

            <View style={styles.container}>

                <View style={{ alignItems: "center", marginTop: 5 }} >
                    <ImageBackground
                        style={{
                            height: 165,
                            width: 165,
                            alignItems: 'flex-end',
                            justifyContent: 'flex-end',
                            backgroundColor: "white",
                            borderRadius: 100,
                            marginTop: 15,
                        }}
                        imageStyle={{ borderRadius: 100 }}
                        source={
                            this.props.set_profile ?
                                { uri: this.props.set_profile.path }
                                : require('../assets/avtar.png')
                        }>

                        <TouchableOpacity
                            onPress={() => this.showAlert()}

                        >
                            <Image
                                style={{ height: 33, width: 33, margin: 10 }}
                                source={require('../assets/camera.png')}
                            />
                        </TouchableOpacity>

                    </ImageBackground>

                    {
                        this.state.loading ?
                            <ActivityIndicator
                                size="small"
                                color="red"
                            /> :
                            null

                    }


                </View>

                <TouchableOpacity
                    // onPress={() => this._onClickSaveChanges()}
                    onPress={()=>alert(this.props.set_profile.path)}
                    style={{ height: 55, backgroundColor: 'green', alignItems: 'center', marginTop: 400, justifyContent: 'center', marginHorizontal: 10 }}
                >
                    <Text style={{ color: 'white' }}>
                        {'Save'}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        // width, height,
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center'
    },
    imgStyle: {
        width: 100,
        height: 100,
    },
});

const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {
        set_profile: s.set_profile
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setProfile: (name) => setProfile(name),
            hitUpdateProfileImageApi: (data) => hitUpdateProfileImageApi(data)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(FormDataApiScreen);
