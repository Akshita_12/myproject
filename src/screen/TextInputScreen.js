import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { isValidEmail, isValidPassword } from '../common/common';
import HooksConceptScreen from '../component/HooksConceptScreen';

class TextInputScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      mobile: '',
      pass: '',

    }
  }


  _onClick = () => {
    // if(valid email)
    if (
      this.state.name == '' ||
      this.state.email == '' ||
      this.state.mobile == '' ||
      this.state.pass == ''
    ) {
      alert('All field must be filled out!');
      return;
    }
    
    if (!isValidEmail(this.state.email)) {
      alert('Please enter valid email address');
      return;
    }
    if (!isValidPassword(this.state.password)) {
      alert('Please enter valid  password');
    // alert('Input Password and Submit [6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter]')
      return;
    }
    if (this.state.mobile.length < 10) {
      alert("Please enter Correct mobile number!")
      return;
    }

    // if (this.props.pass.length < 5) {
    //   alert("You have to enter at least 5 digit password!")
    //   return;
    // }

    // if (this.props.pass.length > 15) {
    //   alert("Password is too long!")
    //   return;
    // }

    // if (this.props.pass != this.props.conf_pass) {
    //   alert("Password is not matching!")
    //   return;
    // }
  }

  // ref_input2 = useRef();
  // ref_input3 = useRef();
  render() {

    return (

      <View style={styles.container}>

        <TextInput
          // placeholder="FirstTextInput"
          placeholder="name"
          returnKeyType="go"
          onSubmitEditing={() => { this.secondTextInput.focus(); }}
          blurOnSubmit={false}
          onChangeText={(text) => this.setState({ name: text })}
          style={{
            height: 55,
            backgroundColor: 'white',
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
            elevation: 6,
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,

          }}
        />

        <TextInput
          placeholder="email"
          returnKeyType="go"
          onSubmitEditing={() => { this.thirdTextInput.focus(); }}
          blurOnSubmit={false}
          onChangeText={(text) => this.setState({ email: text })}
          ref={(input) => { this.secondTextInput = input; }}
          style={{
            height: 55,
            backgroundColor: 'white',
            elevation: 6,
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
          }}
        />

        <TextInput
          // placeholder="ThirdTextInput"
          placeholder="mobile"
          returnKeyType="go"
          maxLength={10}
          onSubmitEditing={() => { this.fourthTextInput.focus(); }}
          blurOnSubmit={false}
          onChangeText={(text) => this.setState({ mobile: text })}
          ref={(input) => { this.thirdTextInput = input; }}
          style={{
            height: 55,
            backgroundColor: 'white',
            elevation: 6,
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
          }}
        />

        <TextInput
          ref={(input) => { this.fourthTextInput = input; }}
          placeholder="password"
          onChangeText={(text) => this.setState({ pass: text })}
          style={{
            height: 55,
            backgroundColor: 'white',
            elevation: 6,
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
          }}
        />


        <TouchableOpacity
            onPress={() => this._onClick()}
            style={{
            height: 60,
            backgroundColor: 'green',
            margin: 10,
            borderRadius: 5,
            justifyContent: 'center',
            marginTop: 60
          }}
        >
          <Text
            style={{ textAlign: 'center', color: 'white', fontWeight: '500', fontSize: 18 }}
          >
            {'Save'}
          </Text>
        </TouchableOpacity>

        <HooksConceptScreen />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center'
  },
  txtStyle: {
    fontSize: 28,
    color: 'blue',
    fontFamily: 'serif'
  },

});

const mapStateToProps = (state) => {
  let getState = state.indexReducer;

  return {
  };
};

const mapDsipatchToProps = (dispatch) => {
  return bindActionCreators(
    {

    },

    dispatch,
  );
};
export default connect(mapStateToProps, mapDsipatchToProps)(TextInputScreen);
