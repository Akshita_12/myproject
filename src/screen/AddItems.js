import React, {Component} from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  StyleSheet,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  setItemsToArray,
  setID,
  setName,
  editItems,
} from '../redux_store/actions/indexActions';

class AddItems extends Component {
  constructor(props) {
    super(props);
  }
  // componentDidMount() {
  //   BackHandler.addEventListener('hardwareBackPress', this.backPressed);
  // }
  // componentDidUnmount() {
  //   BackHandler.addEventListener('hardwareBackPress', this.backPressed);
  // }
  // backPressed = () => {
  //   this.props.navigation.goBack();
  //   return true;
  // };
  render() {
    return (
      <View style={styles.container}>
          <TextInput
          style={styles.nameStyle}
          placeholder="Name..."
          value={this.props.getName}
          onChangeText={(name) => this.props.setName(name)}
        />

        <TextInput
          placeholder="Id"
          value={this.props.getId}
          onChangeText={(id) => this.props.setID(id)}
          style={styles.nameStyle}
          editable={true}
        />
        
        
          <TouchableOpacity
            style={styles.touch}
            onPress={() => {
              this.props.setItemsToArray(this.props.getName, this.props.getId);
              this.props.navigation.goBack();
            }}>
            <Text style={styles.btn}>{'Add Items'}</Text>
          </TouchableOpacity>
        
      </View>
    );
  }
}
let styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  nameStyle: {
    backgroundColor: 'azure',
    elevation: 5,
    margin: 5,
    borderRadius: 5,
    padding: 7,
  },
  touch: {
    alignSelf: 'center',
  },
  btn: {
    backgroundColor: '#8f00ff',
    textAlign: 'center',
    color: 'white',
    marginTop: 20,
    borderRadius: 5,
    paddingEnd: 15,
    paddingStart: 15,
    paddingBottom: 5,
    paddingTop: 5,
  },
});

const mapStateToProps = (state) => {
  const s = state.indexReducer;
  return {
    getName: s.getName,
    getId: s.getId,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setItemsToArray: (name, id) => setItemsToArray(name, id),
      setID: (value) => setID(value),
      setName: (value) => setName(value),
      editItems: (name, id) => editItems(name, id),
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddItems);