import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { shouldUseActivityState } from 'react-native-screens';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSearch, setTemp } from '../redux_store/actions/indexActions';

class FilterScreen extends Component {

    constructor(props) {
        super(props);
        this.props.setTemp(this.props.contact_list)

    }
  
    _renderItem = (item) => {
        return (
            <View style={{
                padding: 15,
                backgroundColor: 'white',
                borderBottomColor: 'light-gray',
                elevation: 7,
                borderBottomWidth: 0.1,
            }}>
                <Text style={{ fontWeight: '600', color: 'gray', fontSize: 16 }}>
                    {item.item.name}
                </Text>
            </View>
        )


    }
    render() {
        return (

            <View style={styles.container}>

                <TextInput
                    placeholder="search here"
                    style={{
                        height: 45,
                        paddingHorizontal: 10,
                        margin: 5,
                        elevation: 8,
                        backgroundColor: 'white'
                    }}
                    onChangeText={(text) =>
                        this.props.setSearch(text)}
                />

                <FlatList
                    data={this.props.contact_list}
                    renderItem={this._renderItem}

                />

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },
    rowStyle: {
        flexDirection: 'row',
        backgroundColor: 'white',
        padding: 20,
        marginHorizontal: 10,
        marginTop: 11,
        elevation: 5,
        borderRadius: 5,
    }
});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        contact_list: getState.contact_list,
        temp: getState.temp
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setSearch: (name) => setSearch(name),
            setTemp: (temp) => setTemp(temp)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(FilterScreen);
