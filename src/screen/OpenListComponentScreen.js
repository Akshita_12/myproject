import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity, Image, Dimensions, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUserImage } from '../redux_store/actions/indexActions';


class OpenListComponentScreen extends Component {

    onImage = () => {
        this.props.setUserImage(this.props.set_user_image),
        this.props.navigation.navigate('OpenImageScreen')
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainViewStyle}>
                    <TouchableOpacity
                        onPress={() =>this.onImage()}
                        style={{
                            padding: 15,
                            backgroundColor: 'green',
                            elevation: 10
                        }}
                    >

                        <Image
                            style={styles.imgStyle}
                            source={this.props.set_user_image}
                        />
                    </TouchableOpacity>

                    <Text style={styles.textStyle1}>{this.props.set_user_details} </Text>


                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width, height,
        backgroundColor: "orange",

    },

    imgStyle: {
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').width,
        // alignSelf: 'center'
    },
    textStyle1: {
        fontSize: 18,
        fontWeight: '900',
        fontFamily: 'serif',
        color: 'magenta',
        margin: 5,
        marginLeft: 10,
        textDecorationLine: 'underline'
    },
    mainViewStyle: {
        flex: 1,
        // backgroundColor: Colors.themeColor
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },


});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        set_user_image: getState.set_user_image,
        set_user_details: getState.set_user_details,
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setUserImage: (img) => setUserImage(img),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(OpenListComponentScreen);
