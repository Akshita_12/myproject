import React, { Component } from 'react'
import { View, Text, StyleSheet, Linking, TouchableOpacity, Pressable, Image, Dimensions, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class OpenImageScreen extends Component {
    constructor(props) {
        super(props);
        this.count = 0;
        this.isTrue = true;
        this.state = {
            width: 100,
            height: 100,
        }
    }

    onImage = () => {
        // this.props.setUserImage(this.props.set_user_image)
        alert(this.props.set_user_details)
    }

    zoomIn = () => {
        if (this.state.height <= 400) {
            this.setState({
                width: this.state.width + 10,
                height: this.state.height + 10
            })
        }
        else {
            return
        }
    }

    zoomOut = () => {
        if (this.state.height >= 50) {
            this.setState({
                width: this.state.width - 10,
                height: this.state.height - 10
            })
        }
    }

    _onDoubleTap = () => {
        this.count = this.count + 1
        setTimeout(() => {
            if (this.count == 2 && this.isTrue == true) {
                this.setState({
                    width: this.state.width + 300,
                    height: this.state.height + 300
                })
                this.count = 0
                this.isTrue = false
            } else if (this.count == 2 && this.isTrue == false) {
                this.setState({
                    width: this.state.width - 300,
                    height: this.state.height - 300
                })
                this.count = 0
                this.isTrue = true
            }
        }, 100)
    }


    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainViewStyle}>
                    <Pressable
                        onPress={() => this._onDoubleTap()}
                        android_disableSound={false}
                        // android_ripple={{ color: 'green' }}
                        style={{
                            // padding: 15,
                            // backgroundColor: 'white',
                            elevation: 9,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >

                        <Image
                            style={[styles.imgStyle,
                            {
                                height: this.state.height,
                                width: this.state.width
                            }
                            ]}
                            source={this.props.set_user_image}
                        />
                    </Pressable>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        position: 'absolute',
                        bottom: 30,
                        left: 25,
                        padding: 15
                    }}>
                        <Pressable
                            onPress={() => this.zoomIn()}
                            android_disableSound={true}
                            android_ripple={{ borderless: true, radius: 20, color: '#2196f3' }}
                            style={{
                                backgroundColor: '#ec407a',
                                elevation: 9,
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 55,
                                width: '40%',
                                elevation: 8,
                                borderRadius: 5,
                                marginRight: 15
                            }}
                        >
                            <Text style={{ fontSize: 20, color: 'white', fontWeight: '600' }}>{'Zoom In'} </Text>

                        </Pressable>

                        <Pressable
                            onPress={() => this.zoomOut()}
                            android_disableSound={false}
                            android_ripple={{ borderless: false, radius: 80, color: '#ffeb3b' }}
                            style={{
                                backgroundColor: '#ec407a',
                                elevation: 9,
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 55,
                                width: '40%',
                                elevation: 8,
                                borderRadius: 5,

                            }}
                        >

                            <Text style={{ fontSize: 20, color: 'white', fontWeight: '600' }}>
                                {'ZoomOut'}
                            </Text>
                            {/* </DoubleClick> */}

                        </Pressable>
                    </View>


                    {/* <Text style={styles.textStyle1}>{this.props.set_user_details} </Text> */}


                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // width, height,
        backgroundColor: "orange",

    },

    imgStyle: {
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').width,
        // alignSelf: 'center'
    },
    textStyle1: {
        fontSize: 18,
        fontWeight: '900',
        fontFamily: 'serif',
        color: 'magenta',
        margin: 5,
        marginLeft: 10,
        textDecorationLine: 'underline'
    },
    mainViewStyle: {
        flex: 1,
        // backgroundColor: Colors.themeColor
        backgroundColor: 'pink',
        justifyContent: 'center',
    },


});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        set_user_image: getState.set_user_image,
        set_user_details: getState.set_user_details,
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(OpenImageScreen);
