import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, TextInput, Alert, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setDeleteRow, setID, setIsChangeText, setItemsToArray, setName, setProfile, setSearch, setTemp } from '../redux_store/actions/indexActions';
import FlatlistScreen_Row from '../screen_row.js/FlatlistScreen_Row';

class FlatlistScreen extends Component {

    constructor(props) {
        super(props);
        this.props.setTemp(this.props.contact_list)

        this.name,
            this.pic,
            this.hashtag,
            this.msg

    }
    onClickRow() {
        this.props.navigation.navigate('AddItems')
    }
    _addItem = () => {
        // this.props.setName('');
        // this.props.setID('');
        this.props.navigation.navigate('AddItems');
    };

    _onAlert = () => {
        alert(this.pic + "\n" + this.hashtag + "\n" + this.msg)
    }
    onClick = (dataItem) => {
        //   alert(dataItem.name)
        this.name = dataItem.name,
            this.pic = dataItem.pic,
            this.hashtag = dataItem.hashTag
        this.msg = dataItem.message
        this.onClickRow()

    }
    _onDelete = (id) => {
        Alert.alert('Delete Contact', 'Are you sure! ', [
            {
                text: 'Cancel',
                style: 'cancel',
                onPress: () => alert('hii i m cancel'),
            },
            {
                text: 'Delete',
                onPress: () => this.props.setDeleteRow(id),
            },
            {
                text: 'test',
            },
        ]);
    };


    _renderItem = (dataItem) => {
        return (
            <FlatlistScreen_Row
                dataItem={dataItem.item}
                onPress={this.onClick}
                onDelete={this._onDelete}
                onClickRow={()=>this.onClickRow()}
            />
        )


    }


    render() {
        return (

            <View style={styles.container}>


                <FlatList
                    data={this.props.user_post}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index + ''}


                />
                <TouchableOpacity
                    style={{ backgroundColor: 'blue', height: 50, marginVertical: 10, marginHorizontal: 10, justifyContent: 'center', alignItems: 'center' }}
                    onPress={() => {
                        this.props.setIsChangeText(!this.props.is_change_text);
                        this._onAlert();

                    }
                    }                >
                    {
                        this.props.is_change_text ?
                            <Text style={{ color: 'white' }}>
                                {'Following'}
                            </Text> :
                            <Text style={{ color: 'white' }}>
                                {'Follow'}
                            </Text>

                    }

                </TouchableOpacity>

                <TouchableOpacity
                     onPress={() => this._addItem()
                    }
                    style={{
                        height: 50,
                        width: 50,
                        backgroundColor: 'red',
                        borderRadius: 100,
                        elevation: 6,
                        bottom: 40,
                        right: 30,
                        position: 'absolute',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Text style={{fontSize:35,color:'white'}}>
                        {'+'}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },
    rowStyle: {
        flexDirection: 'row',
        backgroundColor: 'white',
        padding: 20,
        marginHorizontal: 10,
        marginTop: 11,
        elevation: 5,
        borderRadius: 5,
    }
});

const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {
        contact_list: s.contact_list,
        temp: s.temp,
        user_post: s.user_post,
        is_change_text: s.is_change_text
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setProfile: (name) => setProfile(name),
            setTemp: (temp) => setTemp(temp),
            setIsChangeText: (text) => setIsChangeText(text),
            setDeleteRow: (id) => setDeleteRow(id),
            setItemsToArray: (name, id) => setItemsToArray(name, id),
            setID: (value) => setID(value),
            setName: (value) => setName(value),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(FlatlistScreen);
