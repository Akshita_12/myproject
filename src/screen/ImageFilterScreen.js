import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Animated, Image, TouchableOpacity, Easing, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Video from 'react-native-video';


class ImageFilterScreen extends Component {
    videoPlayer;


    state = {
        loading: true,
        dataSource: [],

        currentTime: 0,
        duration: 0,
        isFullScreen: false,
        isLoading: false,
        paused: false,
        playerState: PLAYER_STATES.PLAYING,
        screenType: 'contain'
    }
    onSeek = seek => {
        this.videoPlayer.seek(seek);
    }

    onPaused = playerState => {
        this.setState({
            paused: !this.state.paused,
            playerState
        });
    }

    onReplay = () => {
        this.setState({ playerState: PLAYER_STATES.PLAYING });
        this.videoPlayer.seek(0);
    }

    onProgress = data => {
        const { isLoading, playerState } = this.state;
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            this.setState({ currentTime: data.currentTime });
        }
    }

    onLoad = data => {
        this.setState({ duration: data.duration, isLoading: false });
    }

    onLoadStart = data => {
        this.setState({ isLoading: true });
    }

    onEnd = () => {
        this.setState({ playerState: PLAYER_STATES.ENDED });
    }

    onError = () => {
        alert(error)
    }

    exitFullScreen = () => {
        alert('Exit full screen')
    }

    enterFullScreen = () => {
        if (this.state.screenType == 'contain') {
            this.setState({ screenType: 'cover' })
        } else {
            this.setState({ screenType: 'contain' })
        }
    }

    renderToolbar = () => (
        <View>
            <Text style={styles.toolbar}> toolbar </Text>
        </View>
    );

    onSeeking = currentTime => {
        this.setState({ currentTime })
    }




    componentDidMount() {
        fetch("http://192.168.0.186:3000/all_video")
            .then(response => response.json())
            .then((response) => {
                this.setState({
                    loading: false,
                    dataSource: response.video_data.data
                })
                // alert(response[0].company.bs)
                console.log(response);
            })
            .catch(error => alert(error))
    }


    _renderItem = (data) => {
        return (
            <View style={{
                padding: 10,
                // width: 353,
            }}>
                <TouchableOpacity
                    onPress={() => {
                    }} >

                    {/* <Image
                        uri={data.item.pictures.sizes[0].link}
                        sources={{uri:data.item.pictures.sizes[0].link}}
                    /> */}
                    <Image source={{ uri: data.item.pictures.sizes[0].link }}
                        style={{ width: '75%', height: 140, alignSelf: 'center' }} />

                    <Image source={{ uri: data.item.pictures.sizes[0].link_with_play_button }}
                        style={{ width: '75%', height: 140, marginTop: 40, alignSelf: 'center' }} />

                    <View style={{ backgroundColor: 'black', justifyContent: 'center', marginTop: 10 }}>
                        <Text style={{ fontSize: 16, fontWeight: '800', color: 'white' }}>
                            {data.item.name}
                        </Text>
                    </View>
                    <Text style={{ fontSize: 16, fontWeight: '800', color: 'black' }}>
                        {data.item.description}
                    </Text>
                    <Text style={{ fontSize: 16, fontWeight: '800', color: 'black' }}>
                        {data.item.link}
                    </Text>
                    {/* <Text style={{ fontSize: 16, fontWeight: '800', color: 'black' }}>
                        {data.item.link_with_play_button}
                    </Text> */}
                </TouchableOpacity>


            </View>
        )
    };
    render() {

        return (

            <View style={styles.container}>
                {/* <Text>{'hiiiiiii'} </Text> */}
                {
                    this.state.loading ?
                        <View style={styles.loader}>
                            <ActivityIndicator size="large" color="#0c9" />
                        </View>
                        :
                        <FlatList
                            data={this.state.dataSource}
                            renderItem={this._renderItem}
                        />

                }

                {/* <View style={{ flex: 1 }}>
                    <Video
                        onEnd={this.onEnd}
                        onLoad={this.onLoad}
                        onLoadStart={this.onLoadStart}
                        onProgress={this.onProgress}
                        paused={this.state.paused}
                        ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                        resizeMode={this.state.screenType}
                        onFullScreen={this.state.isFullScreen}
                        // source={{ uri: 'https://vod-progressive.akamaized.net/exp=1611822123~acl=%2Fvimeo-prod-skyfire-std-us%2F01%[…]prod-skyfire-std-us/01/234/20/501170985/2283165473.mp4', }}
                        source={{ uri: 'https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4', }}
                        style={styles.mediaPlayer}
                        volume={10}
                    />

                    <MediaControls
                        duration={this.state.duration}
                        isLoading={this.state.isLoading}
                        mainColor="#332"
                        onFullScreen={this.onFullScreen}
                        onPaused={this.onPaused}
                        onReplay={this.onReplay}
                        onSeek={this.onSeek}
                        onSeeking={this.onSeeking}
                        playerState={this.state.playerState}
                        progress={this.state.currentTime}
                        toolbar={this.renderToolbar}
                    />
                </View> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },

    mediaPlayer: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
        justifyContent: 'center',
    },
    toolbar: {
        marginTop: 30,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 5
    }

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        // contact_list: getState.contact_list,
        // temp: getState.temp
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setSearch: (name) => setSearch(name),
            // setTemp: (temp) => setTemp(temp)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(ImageFilterScreen);
