import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Snackbar from '../component/Snackbar';

class DisplayScreen extends Component {

    render() {
        return (

            <View style={styles.container}>
              
                <Snackbar
                text="Hello Snackbar"
                 animate={true}
                />
                
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },
   
});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(DisplayScreen);
