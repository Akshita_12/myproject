import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, TextInput } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setIsAutocomplete, setSearchState, setState, setTemp } from '../redux_store/actions/indexActions';

class ApiFetchingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dataSource: []
        }
    }

    onClick = (data) => {
        this.props.navigation.navigate('InfoComponentScreen',{
            name:data.name,
            id:data.id,
            email:data.email,
            city:data.address.city,
            zipcode:data.address.zipcode,
            street:data.address.street,
            suite:data.address.suite
        })
    }

    _renderItem = (data) => {
        return (
            <View style={{
                padding: 15,
                width: 353,
            }}>
                <TouchableOpacity
                    onPress={() => {

                        this.onClick(data.item)
                    }}
                >

                    <Text>
                        {data.item.name}
                    </Text>
                    <Text>
                        {data.item.id}
                    </Text>
                    <Text>
                        {data.item.email}
                    </Text>

                    <Text>
                        {data.item.address.street}
                    </Text>
                    <Text>
                        {data.item.address.suite}
                    </Text>
                    <Text>
                        {data.item.address.city}
                    </Text>
                    <Text>
                        {data.item.address.zipcode}
                    </Text>
                    <Text>
                        {data.item.phone}
                    </Text>

                </TouchableOpacity>


            </View>
        )
    };


    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => response.json())
            .then((response) => {
                this.setState({
                    loading: false,
                    dataSource: response
                })
                // alert(response[0].company.bs)
            })
        // .catch(error => alert(error))
    }

    render() {
        // if(this.state.loading){
        //     return( 
        //       <View style={styles.loader}> 
        //         <ActivityIndicator size="large" color="#0c9"/>
        //       </View>
        //   )}
        return (
            <View style={{
                flex: 1,
            }}>
                {
                    this.state.loading ?
                        <View style={styles.loader}>
                            <ActivityIndicator size="large" color="#0c9" />
                        </View>
                        :
                        <FlatList
                            data={this.state.dataSource}
                            renderItem={this._renderItem}
                        />
                }




            </View>
        );
    }
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        borderStartColor: "#FF1744",
        // backgroundColor:'white'
    },
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    loader: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff"
    },
    list: {
        paddingVertical: 4,
        margin: 5,
        backgroundColor: "#fff"
    }
});
const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setAxiosList: (list) => setAxiosList(list)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(ApiFetchingScreen);
