import React, { Component } from 'react';
import { View, Text, StyleSheet,Animated,Easing } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Snackbar from '../component/Snackbar';

class AnimatedScreen extends Component {
    constructor () {  
        super()  
        this.animatedValue = new Animated.Value(0)  
        console.log("constructor")
    }  

    componentDidMount () {  
        console.log("componentDidmount")

        this.animate()  

    }//animate method is call from componentDidMount  

    animate () {  
        this.animatedValue.setValue(0)  
        Animated.timing(  
            this.animatedValue,  
            {  
                toValue: 1,  
                duration: 2000,  
                easing: Easing.linear  
            }  
        ).start(() => this.animate())  
    }  


    render() {
        console.log("render")

        const marginLeft = this.animatedValue.interpolate({  
            inputRange: [0, 1],  
            outputRange: [0, 300]  
        })  
        const opacity = this.animatedValue.interpolate({  
            inputRange: [0, 0.5, 1],  
            outputRange: [0, 1, 0]  
        })  
        const movingMargin = this.animatedValue.interpolate({  
            inputRange: [0, 0.5, 1],  
            outputRange: [0, 300, 0]  
        })  
        const textSize = this.animatedValue.interpolate({  
            inputRange: [0, 0.5, 1],  
            outputRange: [18, 32, 18]  
        })  
        const rotateX = this.animatedValue.interpolate({  
            inputRange: [0, 0.5, 1],  
            outputRange: ['0deg', '180deg', '0deg']  
        })  

        return (

            <View style={styles.container}>
              
              <Animated.View //returns Animated.View  
                    style={{  
                        marginLeft,  
                        height: 30,  
                        width: 40,  
                        backgroundColor: 'red'}} />  
                <Animated.View  
                    style={{  
                        opacity,  
                        marginTop: 10,  
                        height: 30,  
                        width: 40,  
                        backgroundColor: 'blue'}} />  
                <Animated.View  
                    style={{  
                        marginLeft: movingMargin,  
                        marginTop: 10,  
                        height: 30,  
                        width: 40,  
                        backgroundColor: 'orange'}} />  
                <Animated.Text // returns Animated.Text  
                    style={{  
                        fontSize: textSize,  
                        marginTop: 10,  
                        color: 'green'}} >  
                  {'Animated Text!'}
                </Animated.Text>  
                <Animated.View   
                    style={{  
                        transform: [{rotateX}],  
                        marginTop: 50,  
                        height: 30,  
                        width: 40,  
                        backgroundColor: 'black'}}>  
                    <Text style={{color: 'white'}}>{'Hello from TransformX'}</Text>  
                </Animated.View>  
                
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },
   
});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(AnimatedScreen);
