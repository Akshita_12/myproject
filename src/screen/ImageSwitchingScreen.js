import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Snackbar from '../component/Snackbar';
import { setIsLike } from '../redux_store/actions/indexActions';

class ImageSwitchingScreen extends Component {

    setLike = () => {
        if (this.props.is_like == 0) {
            this.props.setIsLike(1)
        } else {
            this.props.setIsLike(0)
        }
    }
    render() {
        return (

            <View style={styles.container}>

                <TouchableOpacity
                    onPress={() => this.setLike()}
                    style={{ justifyContent: 'center', alignItems: 'center' }}>

                    <Image
                        style={{ height: 40, width: 40 }}
                        source={this.props.is_like == 0 ? require('../assets/unlike.png') :
                            require('../assets/like.png')}
                    />

                </TouchableOpacity>

                {/* <TouchableOpacity 
                // onPress={()=>}
                style={{ justifyContent: 'center', alignItems: 'center' }}>
                    // <Image
                    //     style={{ height: 40, width: 40,tintcolor:'black' }}
                    //     source={require('../assets/like.png')}
                    // />
                </TouchableOpacity> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },
    imgStyle: {
        height: 40,
        width: 40
    }

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        is_like: getState.is_like

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIsLike: (is_like) => setIsLike(is_like)

        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(ImageSwitchingScreen);
