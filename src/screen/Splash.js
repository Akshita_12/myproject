import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPrefs } from '../common/Prefs';
import { setLoginEmail, setLoginPass } from '../redux_store/actions/indexActions';

class Splash extends Component {


  componentDidMount() {
    setTimeout(() => {
      getPrefs('email').then((login_email) => {
        if (login_email != '') {
          this.props.navigation.navigate('Dashboard');

        } else {
          this.props.navigation.navigate('LoginContainer');
        }
      })
    }, 1000);
    setTimeout(() => {
      this.props.navigation.navigate('Dashboard');
    }, 1000);
  }




  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity>
          <Text style={styles.txtStyle} >{'welcome'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtStyle: {
    fontSize: 40,
    color: 'blue',
    fontFamily: 'serif'
  }
});

const mapStateToProps = (state) => {
  let s = state.indexReducer;

  return {
    // dataSource: s.dataSource,
    login_email: s.login_email,
    login_pass: s.login_pass,

  };
};

const mapDsipatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // setAxiosList: (list) => setAxiosList(list)
      setLoginEmail: (login_email) => setLoginEmail(login_email),
      setLoginPass: (login_pass) => setLoginPass(login_pass),
    },

    dispatch,
  );
};
export default connect(mapStateToProps, mapDsipatchToProps)(Splash);
