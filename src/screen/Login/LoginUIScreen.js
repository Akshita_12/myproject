import React from 'react';
import { View, Image, TouchableOpacity, ImageBackground, StyleSheet, TextInput, Platform, Dimensions, KeyboardAvoidingView, SafeAreaView } from 'react-native';
import { Text } from 'react-native';
import MyStrings from '../../common/MyStrings';
import Colors from '../../component/Colors';

// console.log(email_value)

const LoginUIScreen = (props) => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      enabled
      style={{ backgroundColor: Colors.whiteText, flex: 1 }}>

      <SafeAreaView style={styles.container}>

        <View style={styles.mainViewStyle}>

          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>

            <View style={{
              alignItems: "center",
              width: "95%", backgroundColor: "#9c27b0", flexDirection: "row"
            }}>


              <TextInput
                style={{
                  color: 'white', color: "white",
                  height: 50,
                  width: "90%",
                }}
                placeholder={MyStrings.EMAIL_PLACEHOLDER}
                placeholderTextColor="white"
                value={props.email_value}
                onChangeText={(text) => {
                  props.setEmail(text);

                }}
              />
               
              {
                props.is_valid_email ?

                  <Image
                    style={{ width: 30, height: 30 }}
                    source={require("../../assets/right.png")

                    }
                  />
                  :
                  null
              } 

            </View>
            <TextInput style={styles.textInputStyle}
              placeholder={MyStrings.PASSWORD_PLACEHOLDER}
              placeholderTextColor="white"
              value={props.pass_value}
              onChangeText={(text) => { props.setPassword(text) }}
            />

             {props.isLoginDisabled ? 
               null : 
            <TouchableOpacity
              onPress={() => { props.onLoginClick() }}
              style={[styles.btnStyle, { backgroundColor: 'green' }]}  >
              <Text style={{ color: "white" }} >
                {MyStrings.LOGIN_BTN_TEXT}

              </Text>
            </TouchableOpacity>


            }

            <TouchableOpacity
              onPress={() => {
                props.onSignupClick();
              }}
              style={styles.btnStyle}
            >
              <Text style={{ color: "white" }} >
                {MyStrings.SIGNUP_BTN_TEXT}

              </Text>
            </TouchableOpacity>


          </View>
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height,
    width
    // backgroundColor: 'white',
  },
  textInputStyle: {
    padding: 5,
    marginTop: 8,
    color: "white",
    width: "95%",
    height: 55,
    backgroundColor: "#9c27b0",
    elevation: 10
  },
  btnStyle: {
    padding: 5,
    marginTop: 35,
    width: "95%",
    height: 60,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center"
  },
  mainViewStyle: {
    // justifyContent: 'center',
    flex: 1,
  },
});
export default LoginUIScreen;
