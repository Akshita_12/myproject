
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidEmail } from '../../common/common';
import { getPrefs, setPrefs } from '../../common/Prefs';
import { setIsLoginDisabled, setIsValidEmail, setLoginEmail, setLoginPass } from '../../redux_store/actions/indexActions';
import LoginUIScreen from './LoginUIScreen';
import { StackActions } from '@react-navigation/native';
// import { SafeAreaView } from 'react-native-safe-area-context';

class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // email: this.props.route.params.signup_email,
      // pass: this.props.route.params.signup_pass,
      // is_login_disabled: true,
      // is_valid_email: false,
    }
  }

  LoginClick = () => {
    this._onGet()
    alert(this.props.login_pass + this.props.login_email)
    if (this.props.login_email == '' || this.props.login_pass == '') {
      alert('Please enter email and password');
      return;

    }

    this.props.navigation.dispatch(
      StackActions.replace('Dashboard'),
    )
  }

  SignupClick() {
    this.props.navigation.navigate("SignupContainer")

  }
  
  _onGet = () => {
    setPrefs('email', this.props.login_email);
    setPrefs('password', this.props.login_pass);


    // getPrefs('email').then((login_email) => {
    //   if (login_email != '') {
    //     this.props.setLoginEmail(login_email)
        // console.log(login_email);

    //   }
    // })
    // getPrefs('password').then((login_pass) => {
    //   if (login_pass != '') {
    //     this.props.setLoginPass(login_pass)
        // console.log(login_pass);

    //   }
    // })
  }
  

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>

        <LoginUIScreen
          // email_value={this.props.route.params.signup_email}
          // pass_value={this.props.route.params.signup_pass}
          // is_valid_email={this.state.is_valid_email}
          email_value={this.props.login_email}
          pass_value={this.props.login_pass}
          is_valid_email={this.props.is_valid_email}
         

          setEmail={(txt => {
            this.props.setLoginEmail(txt)
            if (txt.length > 6 && isValidEmail(txt)) {
                  this.props.setIsLoginDisabled(false);
                } else {
                  this.props.setIsLoginDisabled(true);
                }

          })}

          setPassword={(value => {
            this.props.setLoginPass(value)


            if (value.length > 6) {
              this.props.setIsLoginDisabled(false);

            } else {
              this.props.setIsLoginDisabled(true);
            }

          })}

          isLoginDisabled={this.props.isLoginDisabled}
          onLoginClick={() => this.LoginClick()}
          onSignupClick={() => this.SignupClick()}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  let s = state.indexReducer;

  return {
    // dataSource: s.dataSource,
    login_email: s.login_email,
    login_pass: s.login_pass,
    isLoginDisabled: s.isLoginDisabled,
    is_valid_email: s.is_valid_email

  };
};

const mapDsipatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // setAxiosList: (list) => setAxiosList(list)
      setLoginEmail: (login_email) => setLoginEmail(login_email),
      setLoginPass: (login_pass) => setLoginPass(login_pass),
      setIsLoginDisabled: (login_disabled) => setIsLoginDisabled(login_disabled),
      setIsValidEmail: (is_valid_email) => setIsValidEmail(is_valid_email)
    },

    dispatch,
  );
};
export default connect(mapStateToProps, mapDsipatchToProps)(LoginContainer);


