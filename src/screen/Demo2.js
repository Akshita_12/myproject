import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking,TextInput,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class Demo2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.route.params.n,
      email: this.props.route.params.e
    }
  }



  render() {

    return (

      <View style={styles.container}>

        <TextInput
          value={this.state.name}
          placeholder="name"
          onChangeText={(text) => this.setState({ name: text })}
          style={{
            height: 55,
            backgroundColor: 'white',
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
            elevation: 6,
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,

          }}
        />

        <TextInput
          value={this.state.email}
          placeholder="email"
          onChangeText={(text) => this.setState({ email: text })}
          style={{
            height: 55,
            backgroundColor: 'white',
            elevation: 6,
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
            elevation:10
          }}
        />


        <TouchableOpacity
          onPress={() =>
            // this._onClick()
            alert(this.state.name + this.state.email)
          }
          style={{
            height: 60,
            backgroundColor: 'green',
            margin: 10,
            borderRadius: 5,
            justifyContent: 'center',
            marginTop: 60,
            elevation:10
          }}
        >
          <Text
            style={{ textAlign: 'center', color: 'white', fontWeight: '500', fontSize: 18 }}
          >
            {'Submit'}
          </Text>
        </TouchableOpacity>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center'
  },
  txtStyle: {
    fontSize: 28,
    color: 'blue',
    fontFamily: 'serif'
  },

});

const mapStateToProps = (state) => {
  let getState = state.indexReducer;

  return {
  };
};

const mapDsipatchToProps = (dispatch) => {
  return bindActionCreators(
    {

    },

    dispatch,
  );
};
export default connect(mapStateToProps, mapDsipatchToProps)(Demo2);
