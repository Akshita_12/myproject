import React, { Component } from 'react';
import { StyleSheet, AppRegistry, Text, View, Animated, Easing } from 'react-native';

export default class AnimatedScreen1 extends Component {
    constructor() {
        super()
        this.animatedValue = new Animated.Value(0)
        console.log("constructor")

    }
    componentWillMount(){
        console.log("componentWillMount")

    }
    componentDidMount() {
        console.log("componentDidMount")

        this.animate()
    }//animate method is call from componentDidMount  
    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 3000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    render() {
        console.log("render")

        const marginLeft = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 900]
        })
        const opacity = this.animatedValue.interpolate({
            inputRange: [0.6, 0.9, 1],
            outputRange: [18, 32, 800]
        })
        const movingMargin = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [18, 32, 900]
        })
        const textSize = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [18, 32, 60]
        })
        const rotateX = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: ['0deg', '180deg', '0deg']
        })
        const rotateY = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: ['0deg', '180deg', '0deg']
        })



        return (
            <View style={styles.container}>
                <Animated.View //returns Animated.View  
                    style={{
                        marginLeft,
                        height: 30,
                        width: 40,
                        backgroundColor: 'red'
                    }} />
                <Animated.View
                    style={{
                        opacity,
                        marginTop: 10,
                        height: 30,
                        width: 40,
                        backgroundColor: 'blue'
                    }} />
                <Animated.View
                    style={{
                        marginLeft: movingMargin,
                        marginTop: 10,
                        height: 30,
                        width: 40,
                        backgroundColor: 'orange'
                    }} />
                <Animated.Text // returns Animated.Text  
                    style={{
                        fontSize: textSize,
                        marginTop: 10,
                        color: 'green'
                    }} >
                    {'Animated Text!'}
                </Animated.Text>
                <Animated.View
                    style={{
                        transform: [{ rotateX }],
                        marginTop: 50,
                        height: 200,
                        width: 350,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'pink'
                    }}>
                    <Text style={{ color: 'gray', fontSize: 20 }}>hlw this is Akshita,.. dhjwedew bdjhd nyuqw</Text>
                </Animated.View>

                <Animated.View
                    style={{
                        transform: [{ rotateY }],
                        marginTop: 50,
                        height: 200,
                        width: 350,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'pink'
                    }}>
                    <Text style={{ color: 'gray', fontSize: 20 }}>hlw this is Akshita,.. dhjwedew bdjhd nyuqw</Text>
                </Animated.View>

                <Animated.Text // returns Animated.Text  
                    style={{
                        fontSize: textSize,
                        marginTop: 10,
                        color: 'green'
                    }} >
                    Akshitaaaaaaaaaaaaaaa!
                </Animated.Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 1
    }
})
// skip this line if you are using Create React Native App  
AppRegistry.registerComponent('DisplayAnImage', () => DisplayAnImage);  