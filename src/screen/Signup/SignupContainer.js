
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSignupEmail, setSignupName, setSignupPass, setSignupPhone, setSignupStateName } from '../../redux_store/actions/indexActions';
import SignupUIScreen from './SignupUIScreen';

class SignupContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // is_signup_disabled: true,
      // is_valid_email: false,
    }
  }

  LoginClick() {
    this.props.navigation.navigate('LoginContainer', {
      signup_email: this.props.signup_email,
      signup_pass: this.props.signup_pass
    })
    // this.props.navigation.navigate('LoginContainer')
  }

  SignupClick() {

    alert(this.props.signup_email)
  }


  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <SignupUIScreen
          name_value={this.props.signup_name}
          email_value={this.props.signup_email}
          phone_value={this.props.phone_value}
          pass_value={this.props.pass_email}
          stateName_value={this.props.stateName_value}

          is_valid_email={this.state.is_valid_email}

          setName={(txt => {
            this.props.setSignupName(txt)

          })}

          setEmail={(txt => {
            this.props.setSignupEmail(txt)

          })}
          setPhone={(txt => {
            this.props.setSignupPhone(txt)

          })}

          setPassword={(txt => {
            this.props.setSignupPass(txt)

          })}

          setStateName={(txt => {
            this.props.setSignupStateName(txt)

          })}
          is_signup_disabled={this.state.is_signup_disabled}


          onSignupClick={() => this.SignupClick()}
          onLoginClick={() => this.LoginClick()}

        />
      </SafeAreaView>
    );
  }
}


const mapStateToProps = (state) => {
  let s = state.indexReducer;

  return {
    // dataSource: s.dataSource,
    signup_name: s.signup_name,
    signup_email: s.signup_email,
    signup_phone: s.signup_phone,
    signup_pass: s.signup_pass,
    signup_state_name: s.signup_state_name

  };
};

const mapDsipatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // setAxiosList: (list) => setAxiosList(list)
      setSignupName: (signup_name) => setSignupName(signup_name),
      setSignupEmail: (signup_email) => setSignupEmail(signup_email),
      setSignupPhone: (signup_phone) => setSignupPhone(signup_phone),
      setSignupPass: (signup_pass) => setSignupPass(signup_pass),
      setSignupStateName: (signup_state_name) => setSignupStateName(signup_state_name),

    },

    dispatch,
  );
};
export default connect(mapStateToProps, mapDsipatchToProps)(SignupContainer);

