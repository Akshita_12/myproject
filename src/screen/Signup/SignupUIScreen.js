import React from 'react';
import { View, TouchableOpacity, StyleSheet, Dimensions, SafeAreaView, TextInput, KeyboardAvoidingView } from 'react-native';
import { Text } from 'react-native';
import MyStrings from '../../common/MyStrings';
import Colors from '../../component/Colors';

const SignupUIScreen = (props) => {
  return (

    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      enabled
      style={{ backgroundColor: Colors.whiteText, flex: 1 }}>
      <SafeAreaView style={styles.container}>
        <View style={styles.mainViewStyle}>

          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <TextInput style={
              styles.textInputStyle
            }
              placeholder={MyStrings.SIGNUP_NAME_PLACEHOLDER}
              placeholderTextColor="white"
              value={props.name_value}
              onChangeText={(text) => {
                props.setName(text);

              }}
            />

            <TextInput style={
              styles.textInputStyle
            }
              placeholder={MyStrings.SIGNUP_EMAIL_PLACEHOLDER}
              placeholderTextColor="white"
              value={props.email_value}
              onChangeText={(text) => {
                props.setEmail(text);
              }}
            />
            <TextInput style={
              styles.textInputStyle
            }
              placeholder={MyStrings.SIGNUP_PHONE_PLACEHOLDER}
              placeholderTextColor="white"
              value={props.phone_value}
              onChangeText={(text) => { props.setPhone(text) }}

            />

            <TextInput style={
              styles.textInputStyle
            }
              placeholder={MyStrings.SIGNUP_PASS_PLACEHOLDER}
              placeholderTextColor="white"
              value={props.pass_value}
              onChangeText={(text) => { props.setPassword(text) }}

            />

            <TextInput style={
              styles.textInputStyle
            }
              placeholder={MyStrings.SIGNUP_STATE_NAME_PLACEHOLDER}
              placeholderTextColor="white"
              value={props.stateName_value}
              onChangeText={(text) => { props.setStateName(text) }}

            />




            <TouchableOpacity
              onPress={() => {
                props.onSignupClick();
              }}
              style={[styles.btnStyle,{backgroundColor:'magenta'}]}
            >
              <Text style={{ color: "white", fontSize: 20, fontWeight: '800' }} >
                {MyStrings.SIGNUP_BTN_TEXT}

              </Text>
            </TouchableOpacity>


            <TouchableOpacity

              style={styles.btnStyle}
              onPress={() => {
                props.onLoginClick();
              }}
            >
              <Text style={{ color: "white", fontSize: 20 }}>
                {MyStrings.LOGIN_BTN_TEXT}
              </Text>

            </TouchableOpacity>


          </View>
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height,
    width
    // backgroundColor: 'white',
  },
  textInputStyle: {
    padding: 5,
    marginTop: 8,
    color: "white",
    width: "95%",
    height: 55,
    backgroundColor: "#9c27b0",
    elevation: 10
  },
  btnStyle: {
    padding: 5,
    marginTop: 35,
    width: "95%",
    height: 60,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center"
  },
  mainViewStyle: {
    // justifyContent: 'center',
    flex: 1,
  },
});
export default SignupUIScreen;
