import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Footer from '../component/Footer';
import Header1 from '../component/Header1';
import { Rating, AirbnbRating } from 'react-native-ratings';

class DemoUI extends Component {
    _onGoBack = () => {
        this.props.navigation.goBack()
    }

    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>

                    <Header1
                        onGoBack={() => this._onGoBack()}
                        isCross={true}
                        barStyle='dark-content'
                        tintColor={'white'}
                    />

                    <View style={{
                        width: '100%',
                        flex: 1,
                        borderRadius: 14,
                        backgroundColor: "white",
                        elevation: 5
                    }}>
                        <TouchableOpacity>
                            <Image
                                style={styles.imgStyle}
                                source={require('../assets/chair1.png')}
                            />
                        </TouchableOpacity>

                        <Text style={styles.txtStyle}>
                            {'Dining Chair'}
                        </Text>
                        <AirbnbRating
                            count={7}
                            reviews={["Terrible", "Very Bad", "Bad", "OK", "Fair", "Good", "Very Good", "Wow", "Amazing", "Unbelievable", "Excellent"]}
                            defaultRating={5}
                            size={18}
                            onFinishRating={this.ratingCompleted}

                        />

                        <Text style={[styles.txtStyle, { paddingHorizontal: 15, fontSize: 14, color: '#bdbdbd' }]}>
                            {`A chair is a piece of furniture. It is used for sitting on and it can also be used for standing on, if you can't reach something. They usually have four legs to support the weight. Some types of chairs, such as the bar-stool, have only one leg in the center. Those chairs are usually able to spin.`}
                        </Text>

                        <View style={{ height: 2, backgroundColor: 'light-gray', marginHorizontal: 10, width: '94%', marginVertical: 10 }} />
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text style={[styles.txtStyle, { color: '#bdbdbd' }]}>
                                {'$280'}
                            </Text>
                            <View style={{ flexDirection: 'row', height: 45, marginRight: 25, width: '25%', borderRadius: 12, alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#bdbdbd' }}>

                                <Text style={{ color: '#3D3D3D', fontWeight: 'bold', marginLeft: 15 }}>
                                    {'CART'}
                                </Text>
                                <Text style={{ color: '#3D3D3D', fontWeight: 'bold', marginRight: 15 }}>
                                    {'+'}
                                </Text>

                            </View>
                        </View>


                        <View style={{
                            width: '15%',
                            backgroundColor: '#ffffff',
                            elevation: 0,
                            height: 300,
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            borderRadius: 10,
                            // marginTop:45


                        }}>
                            <Image
                                style={{ height: 25, width: 25, marginTop: 15, tintColor: '#737373' }}
                                source={require('../assets/lock_g.png')}
                            />
                            <Image
                                style={{ height: 30, width: 30, marginTop: 15, tintColor: '#ff80ab' }}
                                source={require('../assets/like.png')}
                            />
                            <TouchableOpacity style={{}}>
                                <View
                                    style={{ backgroundColor: 'purple', marginTop: 100, height: 15, width: 15, borderRadius: 100 }}
                                />
                                <View
                                />
                                <View
                                    style={{ backgroundColor: 'brown', height: 15, width: 15, marginTop: 15, borderRadius: 100 }}
                                />
                                <View
                                    style={{ backgroundColor: 'orange', height: 15, width: 15, marginTop: 15, borderRadius: 100 }}
                                />
                            </TouchableOpacity>


                        </View>
                    </View>
                    <Footer
                        title="BUY NOW"
                        onGoBack={() => this._onGoBack()}
                    />


                </View>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#37474f',
    },
    txtStyle: {
        fontSize: 23,
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontWeight: 'bold'
    },
    imgStyle: {
        height: 300,
        width: 300,
        alignSelf: 'center'
    }

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(DemoUI);
