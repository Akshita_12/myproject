import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Rating, AirbnbRating } from 'react-native-ratings';





class RatingScreen extends Component {

    WATER_IMAGE = require('../assets/drop1.png')

    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }

    _onGoBack = () => {
        this.props.navigation.goBack()
    }
    render() {
        return (

            <View style={styles.container}>
                {/* <Header1
                    onGoBack={() => this._onGoBack()}
                    isCross={true}
                    barStyle='dark-content'
                    tintColor={'white'}
                /> */}

                <AirbnbRating />
                <AirbnbRating
                    count={11}
                    reviews={["Terrible", "Bad", "Meh", "OK", "Good", "Hmm...", "Very Good", "Wow", "Amazing", "Unbelievable", "Jesus"]}
                    defaultRating={11}
                    size={20}
                />

                <Rating
                    showRating
                    onFinishRating={this.ratingCompleted}
                    style={{ paddingVertical: 10 }}
                />

                <Rating
                    type='heart'
                    ratingCount={3}
                    imageSize={60}
                    showRating
                    onFinishRating={this.ratingCompleted}
                />

                <Rating
                    type='custom'
                    ratingImage={this.WATER_IMAGE}
                    ratingColor='#3498db'
                    // ratingBackgroundColor='#c8c7c8'
                    ratingCount={9}
                    imageSize={30}
                    onFinishRating={this.Image}
                    style={{ paddingVertical: 10 }}
                />


            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },

});

const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(RatingScreen);
