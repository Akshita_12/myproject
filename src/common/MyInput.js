import React from 'react';
import { TextInput } from 'react-native';
import { View, Image, TouchableOpacity } from 'react-native';

const MyInput = (props) => {
  return (
    <View style={{
      flexDirection: 'row',
      height: 45,
      backgroundColor: 'white',
      elevation: 5,
      borderRadius: 4,
      alignItems: 'center',
      justifyContent: 'flex-start',
      marginHorizontal:8,
      marginTop:5
    }}>
      <TouchableOpacity style={{ alignItems: "center" }}>
        <Image
          style={{ height: 20, width: 20, marginHorizontal: 8 }}
          source={require('../assets/search.png')}

        />
      </TouchableOpacity>

      <TextInput
        placeholder="Search for Products,Brands and More "
        paddingHorizontal={3}
        multiline={props.multiline}
        onChangeText={props.onChangeText}

        style={{
          // height: 50,
          backgroundColor: 'white',
        }}


      />
    </View>

  );
};

export default MyInput;
