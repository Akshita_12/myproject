import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import React from 'react';
import MyFont from './MyFont';

const MyButton = (props) => {
  return (
    <TouchableOpacity
    onPress={props.onPress}
    style={[styles.myshadow,
    {
          backgroundColor: '#03a9f4',
          height: props.height,
          borderRadius: props.borderRadius,
          elevation: props.elevation,
          width:props.width,
          alignItems: 'center',
          // justifyContent: 'center',
          marginHorizontal: props.marginHorizontal,
          alignSelf:props.alignSelf,
          marginBottom:props.marginBottom
        },
        props.style,
      ]}>
      <Text
        style={{
          fontSize: props.fontSize,
          color: 'white',
          fontFamily: MyFont.Bold,
          fontWeight: 'bold',
          marginTop:5,
          textAlign:'center'
        }}>
        {props.label}{' '}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({

  myshadow:{
    shadowColor:"#1050e6",
    shadowOpacity:0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    // elevation: 2,
  },

  imageIconStyle: {
    // height: 25,
    // marginLeft: 10,
    // width: 25,
    // borderRadius: 30,
    // backgroundColor: "#48BB36",
    // justifyContent: "center",
    // alignItems: 'center'
  },

});
export default MyButton;
