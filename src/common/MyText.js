import React from 'react';
import {View, Text} from 'react-native';
import MyFont from './MyFont';
const MyText = (props) => {
  return (
    <Text
      style={[
        {
          fontSize: 20,
          fontFamily: MyFont.Italics,
          color: 'black',
        },
        props.style,
      ]}>
      {props.value}
    </Text>
  );
};
export default MyText;
