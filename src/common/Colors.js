const tintColor = '#66ffff';
const whiteText = '#ffffff';
const statusBarColor = '#1050E6';
const backgroundColor = '#1050E6';
const placeHolderColor = '#C4C4C4';
const textColor = '#343434';
const textDarkColor = "#3D3D3D"
const themeColor = '#37474f';
const grayColor = '#737373';
const headerColor = '#c5c8cd';
const lightgray = '#bdbdbd'
const yellowColor = '#bbd23c';
const textDullColor = '#bdbdbd';
const red = "#a83b3b";
export default {
  tintColor,
  tabIconDefault: '#384659',
  tabIconSelected: tintColor,
  whiteText,
  themeColor,
  statusBarColor,
  red,
  grayColor,
  backgroundColor,
  placeHolderColor,
  textColor,
  textDarkColor,
  headerColor,
  lightgray,
  yellowColor,
  textDullColor
};
