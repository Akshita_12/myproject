import Constants from "./Constants";








export default {
   EMAIL_PLACEHOLDER: "Enter Email",
   PASSWORD_PLACEHOLDER: "Enter Password",
   LOGIN_BTN_TEXT: "LOGIN",
   SIGNUP_BTN_TEXT: "SIGNUP",

   SIGNUP_NAME_PLACEHOLDER: 'Enter name...',
   SIGNUP_EMAIL_PLACEHOLDER: 'Enter email...',
   SIGNUP_PHONE_PLACEHOLDER: 'Enter phone no',
   SIGNUP_PASS_PLACEHOLDER: 'Enter password',
   SIGNUP_STATE_NAME_PLACEHOLDER: 'Enter state'
};