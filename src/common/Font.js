const Roboto = 'Roboto';
const LAOUI = 'LaoUI';
const LAOUI_BOLD = 'LaoUI-Bold';
const DancingScipt = 'DancingScript-SemiBold';
const fontSize = 14;
const SourceSansPro = 'SourceSansPro-SemiBold';
const SourceSansProLight = 'SourceSansPro-Light';
const myFont ='SourceSansPro-Bold';
const robotoSourceSansPro  = Platform.OS =='android'?'Roboto':'SourceSansPro-Bold';
const robotoSourceSansProBold = Platform.OS =='android'?'Roboto-Bold':'SourceSansPro-Bold';


export default {
  Roboto,
  LAOUI,
  LAOUI_BOLD,
  DancingScipt,
  fontSize,
  inputFont: 17,
  SourceSansPro,
  myFont,
  SourceSansProLight,
  robotoSourceSansPro,
  robotoSourceSansProBold
};
