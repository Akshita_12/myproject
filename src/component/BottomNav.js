import { TouchableOpacity, Text, View, StyleSheet, Image, } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { Icon } from 'react-native-elements';

const BottomNav = (props) => {

  const _onShopStore = () => {
    props.navigation.navigate('StoreScreen', { url: 'https://boxprotraining.co.uk/index.php/shop/' })
  }


  return (
    <View style={{
      height: 70,
      backgroundColor:Colors.yellowColor,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}>
      {/* <Pressable
                android_ripple={{ radius: 100, color: Colors.yellowColor }} */}

      < TouchableOpacity
        onPress={() => props.navigation.navigate('HomeScreen')}
        style={styles.touch}
      >
        <Image
          source={require('../../assets/image/home.png')}
          style={[styles.imgStyle, { tintColor: props.isHome ? '#d04766' : Colors.themeColor }]}
        />

        <Text style={[styles.txtStyle, { color: props.isHome ? '#d04766' : Colors.themeColor }]}>{'Home'} </Text>
      </TouchableOpacity>


      <TouchableOpacity
        onPress={() => props.navigation.navigate('ActivityScreen')}
        style={styles.touch}>
        <Image
          source={require('../../assets/image/activity.png')}
          style={[styles.imgStyle, { tintColor: props.isActivity ? '#d04766' : Colors.themeColor }]}
        />

        <Text style={[styles.txtStyle, { color: props.isActivity ? '#d04766' : Colors.themeColor }]}>{'Activity'} </Text>
      </TouchableOpacity>



      <TouchableOpacity style={styles.touch}
        onPress={() => props.navigation.navigate('ProfileScreen')}
      >
        <Image
          source={require('../../assets/image/profile.png')}
          style={[styles.imgStyle, { tintColor: props.isProfile ? '#d04766' : Colors.themeColor }]}
        />

        <Text style={[styles.txtStyle, { color: props.isProfile ? '#d04766' : Colors.themeColor }]}>{'Profile'} </Text>
      </TouchableOpacity>



      <TouchableOpacity
        onPress={() => _onShopStore()}

        style={styles.touch}>
        <Image
          source={require('../../assets/image/store.png')}
          style={[styles.imgStyle, { tintColor: props.isStore ? '#d04766' : Colors.themeColor }]}
        />


        <Text style={[styles.txtStyle, { color: props.isStore ? '#d04766' : Colors.themeColor }]}>{'Store'} </Text>
      </TouchableOpacity>



      <TouchableOpacity
        onPress={() => props.navigation.navigate('MoreScreen')}
        style={styles.touch}>
        <Image
          source={require('../../assets/image/more.png')}
          style={[styles.imgStyle, { tintColor: props.isMore ? '#d04766' : Colors.themeColor }]}

        />

        <Text style={[styles.txtStyle, { color: props.isMore ? '#d04766' : Colors.themeColor }]}>{'More'} </Text>
      </TouchableOpacity>

    </View>
  );
};
const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  imgStyle: {
    width: 30,
    height: 25,
    // resizeMode:'cover'
  },
  txtStyle: {
    fontSize: 11,
    fontWeight: '500',
    marginTop: 5,
    fontFamily: Font.robotoSourceSansProBold
  },
  touch: {
    height: 50,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
export default BottomNav;
