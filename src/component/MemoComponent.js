import React from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity, Image, SafeAreaView } from 'react-native';

export function Movie(props) {
  return (
    <View>
    <Text>Movie title: {props.title}</Text>
    <Text>Release date: {props.releaseDate}</Text>
  </View>
  );
}

export const MemoizedMovie = React.memo(Movie);