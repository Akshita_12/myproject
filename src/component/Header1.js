import { TouchableOpacity, Text, View, StyleSheet, Image, ToastAndroid, StatusBar, Dimensions } from 'react-native';
import React from 'react';
import Font from '../common/Font';

const Header1 = (props) => {
  return (
    <View style={{
      height: 60,
      elevation: 2,
      backgroundColor: "#37474f",
      flexDirection: 'row',
      alignItems: "center",
      width: Dimensions.get('window').width,
      justifyContent: "center"
    }}>


      {props.isCross ?
        <TouchableOpacity
          onPress={props.onGoBack}
          style={{ width: 40, height: 40, justifyContent: "center", position: "absolute", left: 5, alignItems: "center", alignSelf: 'baseline' }}
        >
          <Image
            source={props.image ? props.image : require("../assets/back.png")}
            style={{ width: 30, height: 15, tintColor: props.tintColor }}
          />
        </TouchableOpacity> : null}

      <Text style={styles.textHeaderStyle}>
        {props.title}
      </Text>


    </View>
  );
};

const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  textHeaderStyle: {
    fontSize: 19,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#737373',
  },
});
export default Header1;
