import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';

const Header = (props) => {
  return (
    <View style={[styles.container, { justifyContent: 'space-between' },props.containerStyle]}>
      <View style={[styles.container,props.mainContainer]}>
        <TouchableOpacity onPress={props.onPress}>
          {
            !props.isDisableBack ?
              <Image
                style={{
                  width: props.iconWidth,
                  resizeMode: "contain",
                  marginHorizontal: 11,
                  height: props.iconHeight,
                }}
                source={require('../assets/back.png')}
              /> : null
          }
        </TouchableOpacity>
        <Text style={{ color: props.color, fontSize: 20,fontWeight:'bold', marginLeft: 10 }}>
          {props.status}
        </Text>
      </View>
      <TouchableOpacity onPress={props.onClick}>
        <Image
          source={props.image}
          style={{ width: 25, marginRight: 10, height: 25 }}
        />
      </TouchableOpacity>

      { props.isEditDelete?
        <View style={{flexDirection:"row"}} >
          <TouchableOpacity onPress={props.onEdit}>
            <Image
              source={props.edit_icon}
              style={{ width: 20, marginRight: 10, height: 20 }}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={props.onDelete}>
            <Image
              source={props.delete_icon}
              style={{ width: 20, marginRight: 10, height: 20 }}
            />
          </TouchableOpacity>
        </View>
        :null
      }

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    backgroundColor: '#37474f',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default Header;
