import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Dimensions, ImageBackground, FlatList } from 'react-native';
import Colors from '../common/Colors';
import VideoTitle from './VideoTitle';

export const BoxproFreePremium = (props) => {

    const _renderItem = (dataItem) => {
        return (
            <View
                key={dataItem.index}
                style={{
                    marginBottom: 10,
                    flex: 1,
                    marginLeft: 5,
                    marginRight: 5,
                    elevation: 10
                }}
            >

                <TouchableOpacity
                    onPress={() => props.navigation(dataItem.item)}
                    style={{
                        padding: 3,
                        backgroundColor: 'rgb(42, 56, 65)',
                        borderRadius: 3,
                    }}
                >
                    <ImageBackground
                        source={dataItem.item.thumb}
                        imageStyle={{
                            borderRadius: 2,
                        }}
                        style={{
                            flex: 1,
                            height: 85,
                            justifyContent: "center",
                            alignItems: 'center',
                        }}
                    >
                        <Image
                            source={require('../../assets/image/play1.png')}
                            style={{ height: 30, width: 30 }}
                        />
                    </ImageBackground>
                </TouchableOpacity>

            </View>
        )
    }

    return (
        < View style={{ marginHorizontal: 15 }} >

            <VideoTitle
                onSeeAll={()=>props.onSeeAll(props.list)}
                type={props.type} />
            <FlatList
                data={props.list}
                renderItem={_renderItem}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => "key" + index}
                numColumns={2}
            />

        </View >
    );
};
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    viewPager: {
        height: 250,

    },
    txtStyle: {
        fontSize: 16,
        color: Colors.whiteColor
    },
    pagerStyle: {
        flex: 1,
        elevation: 5,
        backgroundColor: 'cyan',
        alignItems: "center",
        justifyContent: 'center'
    }
});