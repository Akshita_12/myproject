import React, { useEffect, useState } from 'react';
import { View, Text,FlatList,StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const UseEffectFetchAPI = (props) => {

    const [movie_list, setMovie] = useState([])
    useEffect(() => {
        fetch('https://reactnative.dev/movies.json')
            .then((response) => response.json())
            .then((json) => {
                console.log(json.movies);
                setMovie(json.movies)
            })
            .catch((error) => {
                console.error(error);
            });
    })

    const renderItem = (dataItem) => {
        return (
            <View style={{flex:1,backgroundColor:'white',elevation:11}} >
               <TouchableOpacity>
                <Text>
                    {dataItem.item.title}
                </Text>
                <Text>
                    {dataItem.item.id}
                </Text>
                <Text>
                    {dataItem.item.releaseYear}
                </Text>
                </TouchableOpacity>
            </View>
        )


    }
    return (
        <View style={styles.container}>
            <FlatList
                data={movie_list}
                renderItem={renderItem}
            />
        </View>
    )
}

const styles=StyleSheet.create({
    // container:{
    //     flex:1,
    //     justifyContent:'center'
    // }
})
export default UseEffectFetchAPI;