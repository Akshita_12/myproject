

import React, { Component } from "react";
import {
  StyleSheet,

  Dimensions,

  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  FlatList,

  View, ActivityIndicator, Platform

} from "react-native";
import { debugLog } from "../common/Constants";
import MyInputText from "./MyInputText";

// import { textColor, hintColor, borderColor } from "./../other/MyConst";

export default class MyAutoComplete extends Component {
  constructor(props) {
    super(props);
  }

  //AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);

  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  _renderItem = item => this.props.renderItem(item); z

  _keyExtractor = (item, index) => "item.id" + index;
  render() {
    debugLog(this.props.data)
    return (
      <View style={[{ alignItems: "center" }, Platform.OS == "ios" ? styles.ios : styles.androidStyle]}>
        <View
          style={[{ flexDirection: "row", alignItems: "center" }, this.props.style]}

        >


          {/* <MyInputText
            label={this.props.hintText}
            style={{ width: this.getWidth(275) }}
            left_icon={require('../assets/lock_g.png')}
            value={this.props.value}
          /> */}

          {/* <View style={{ width: 20, height: 20 }}>
            {!this.props.loading ? (
              <View />
            ) : (
                <ActivityIndicator
                  size={"small"}
                  color={"blue"}
                />
              )}
          </View> */}

        </View>

        {/* {this.props.data.length > 0 ? */}
        <View style={[{ backgroundColor: "#E5E5E5", elevation: 5, height: 300, position: "absolute" }, styles.shadow]}>
          <FlatList
            style={{ width: this.getWidth(287) }}
            data={this.props.data}
            renderItem={this._renderItem}
            keyboardShouldPersistTaps="always"
            keyExtractor={this._keyExtractor}
          />

        </View>
        {/* : null */}
        {/* } */}

      </View>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    width,
    height,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fffa"
  },
  myText: {
    color: "white",
    marginTop: 10
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },

    shadowOpacity: 0.2,

    elevation: 5
  },
  iosStyle: { zIndex: 5 },
  androidStyle: {

  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});