import React, {useState} from "react"
import { View, Text,TouchableOpacity } from "react-native"

function HooksConceptScreen() {
  const [count, setCount] = useState(0);

  // console.log(count);
  return (

    <View>
      <Text>{'You clicked count times'} </Text>

      <TouchableOpacity onPress={() => setCount(count + 2)}>
        <Text>{'Click me' } </Text>
        <Text>{count} </Text>
      </TouchableOpacity>
    </View>

  )
}
// ReactDOM.render(
//   <MyApp />,

//   document.getElementById("root")

export default HooksConceptScreen;