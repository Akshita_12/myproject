import { TouchableOpacity, Text, View, StyleSheet, Image, ToastAndroid, StatusBar, Dimensions } from 'react-native';
import React from 'react';
import Font from '../common/Font';

const Footer = (props) => {
  return (
    <View style={{
      height: 60,
      elevation: 2,
      backgroundColor: '#37474f',
      flexDirection: 'row',
      alignItems: "center",
      width: Dimensions.get('window').width,
      justifyContent: "space-around"
    }}>

      <Text style={styles.textHeaderStyle}>
        {props.title}
      </Text>
      <Image
        style={{ height: 20, width: 20,tintColor:'white' }}
        source={require('../assets/next1.png')}
      />



    </View>
  );
};

const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  textHeaderStyle: {
    fontSize: 19,
    fontWeight: 'bold',
    textAlign: 'center',
    color: "white",
  },
});
export default Footer;
