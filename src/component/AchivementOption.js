import React from 'react'
import {
    Dimensions, Image, Platform, Pressable, StyleSheet, Text,
    ToastAndroid, TouchableOpacity, View
} from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const AchivementOption = (props) => {
    return (

        <TouchableOpacity
            android_ripple={{ radius: 100, color: Colors.yellowColor }}
            onPress={() => props.navigation(props.data_item)}
            style={[styles.mainViewStyle,styles.myshadow
            ]}>

            <Image
                source={props.data_item.thumb}
                style={{
                    width: Dimensions.get('window').width / 2.3,
                    height: Dimensions.get('window').width / 5,
                    borderRadius: 2
                }}
            />
        </TouchableOpacity>

    )
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        backgroundColor: 'white',
    },
    imgStyle: {
        width: getWidth(120),
        height: getWidth(120),
    },

    textStyle: {
        fontSize: 14,
        fontWeight: '600',
        fontFamily: Font.Roboto,
        textAlign: 'center',
        color: "gray"
    },
    mainViewStyle: {
        justifyContent: 'center',
        width: Dimensions.get('window').width / 2.3,
        height: Dimensions.get('window').width / 5,
        alignItems: 'center',
        padding: 15,
        margin: 5,
        backgroundColor: "white",
        borderRadius: 3,
        elevation: 5,
        zIndex: 1,
    },
    myshadow:{
        shadowColor:"#1050e6",
        shadowOpacity:0.15,
        
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
      },
});

export default AchivementOption;