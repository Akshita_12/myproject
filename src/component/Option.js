import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { getWidth } from './Layout'

const Option = (props) => {
    return (
        <TouchableOpacity 
        onPress={props.onPress}
        style={[styles.container,styles.myshadow]} >
            <Text style={styles.txtStyle} >
                {props.option_name}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: getWidth(100),
        height: getWidth(100),
        backgroundColor: "white",
        borderRadius: 5,
        justifyContent:"center",
        padding: 5,
        margin: 10,
        alignItems:'center'
    },
    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
    },
    txtStyle:{
        fontSize:18,
        fontWeight:'bold',
        textAlign:"center",
        alignSelf:'center',
        color:'green',
        fontFamily:'serif'
    }
})

export default Option;