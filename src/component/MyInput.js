import React from 'react';
import { View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';


const MyInput = (props) => {
  return (
      <TextInput
        value={props.placeholder}
        style={{
          backgroundColor:'white'
        }}
      />
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    backgroundColor: '#448AFF',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default MyInput;
