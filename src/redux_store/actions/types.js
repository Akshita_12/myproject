export const MODULE_KEY = 'module_app_store_';

export const  FILTER_DATA = MODULE_KEY + 'FILTER_DATA';

export const SET_SEARCH = MODULE_KEY + 'SET_SEARCH';
export const  SET_TEMP = MODULE_KEY + 'SET_TEMP';


export const IS_AUTOCOMPLETE = MODULE_KEY + 'IS_AUTOCOMPLETE';
export const SET_SEARCH_STATE = MODULE_KEY + 'SET_SEARCH_STATE';
export const SET_STATE = MODULE_KEY + 'SET_STATE';


export const  SET_USER_IMAGE = MODULE_KEY + 'SET_USER_IMAGE';
export const  SET_USER_DETAILS = MODULE_KEY + 'SET_USER_DETAILS';
export const  IS_LIKE = MODULE_KEY + 'IS_LIKE';


// **************************** signup ******************************//

export const SET_SIGNUP_NAME = MODULE_KEY + 'SET_SIGNUP_NAME';
export const SET_SIGNUP_EMAIL = MODULE_KEY + 'SET_SIGNUP_EMAIL';
export const SET_SIGNUP_PHONE = MODULE_KEY + 'SET_SIGNUP_PHONE';
export const SET_SIGNUP_PASS = MODULE_KEY + 'SET_SIGNUP_PASS';
export const SET_SIGNUP_STATE_NAME = MODULE_KEY + 'SET_SIGNUP_STATE_NAME';

// **************************** login ******************************//

export const SET_LOGIN_PASS = MODULE_KEY + 'SET_LOGIN_PASS';
export const SET_LOGIN_EMAIL = MODULE_KEY + 'SET_LOGIN_EMAIL';

export const IS_LOGIN_DISABLED = MODULE_KEY + 'IS_LOGIN_DISABLED';
export const IS_VALID_EMAIL = MODULE_KEY + 'IS_VALID_EMAIL';

export const IS_CHANGE_TEXT = MODULE_KEY + 'IS_CHANGE_TEXT';

export const SET_PROFILE = MODULE_KEY + 'SET_PROFILE';

export const DELETE_ROW = MODULE_KEY + 'DELETE_ROW';
export const SET_ITEMS_TO_ARRAY = MODULE_KEY + 'SET_ITEMS_TO_ARRAY';
export const ON_NAME = MODULE_KEY + 'ON_NAME';
export const ON_ID = MODULE_KEY + 'ON_ID';






