import store from "../store"
import { DELETE_ROW, FILTER_DATA, IS_AUTOCOMPLETE, IS_CHANGE_TEXT, ON_ID, ON_NAME, IS_LIKE, IS_LOGIN_DISABLED, IS_VALID_EMAIL, SET_LOGIN_EMAIL, SET_LOGIN_PASS, SET_NAME, SET_PROFILE, SET_SEARCH, SET_SEARCH_STATE, SET_SIGNUP_EMAIL, SET_SIGNUP_NAME, SET_SIGNUP_PASS, SET_SIGNUP_PHONE, SET_SIGNUP_STATE_NAME, SET_STATE, SET_TEMP, SET_USER_DETAILS, SET_USER_IMAGE, SET_ITEMS_TO_ARRAY } from "./types"

export const setFilterData = (name) => (dispatch) => {



  dispatch({
    type: FILTER_DATA,
    payload: name
  })
}

export const setSearch = (txt) => (dispatch) => {
  let state = store.getState().indexReducer
  let temp = []
  temp = state.contact_list
  let lower = txt.toLowerCase();
  let filterData = state.contact_list.filter((item) => {
    let name = item.name.toLowerCase();
    return name.includes(lower)
  })
  if (lower != '') {
    dispatch({
      type: SET_SEARCH,
      payload: filterData
    })
  }
  else {
    dispatch({
      type: SET_SEARCH,
      payload: state.temp
    })
  }

}

export const setTemp = (temp) => (dispatch) => {
  dispatch({
    type: SET_TEMP,
    payload: temp

  })
}



export const setIsAutocomplete = (is_autocomplete) => (dispatch) => {
  dispatch({
    type: IS_AUTOCOMPLETE,
    payload: is_autocomplete,
  });
};

export const setSearchState = (name) => (dispatch) => {
  // console.log(FilterData)
  let state = store.getState().indexReducer
  let temp = []
  temp = state.state_list
  let search_state = name.toLowerCase();
  let FilterData = state.state_list.filter((item) => {
    let name = item.name.toLowerCase();
    return name.includes(search_state)
  })
  if (search_state == '') {
    dispatch({

      type: SET_SEARCH_STATE,
      payload: state.temp
    })
  }
  else {
    dispatch({
      type: SET_SEARCH_STATE,
      payload: FilterData
    })
  }
}

export const setUserImage = (img) => (dispatch) => {
  console.log(img);
  dispatch({
    type: SET_USER_IMAGE,
    payload: img

  })
}
export const setUserDetails = (details) => (dispatch) => {
  dispatch({
    type: SET_USER_DETAILS,
    payload: details

  })
}

export const setIsLike = (like) => (dispatch) => {
  dispatch({
    type: IS_LIKE,
    payload: like

  })
}

export const setState = (state) => (dispatch) => {
  dispatch({
    type: SET_STATE,
    payload: state

  })
}


// **************************** signup ******************************//
export const setSignupName = (signup_name) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_NAME,
    payload: signup_name,
  });
};
export const setSignupEmail = (signup_email) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_EMAIL,
    payload: signup_email,
  });
};
export const setSignupPhone = (signup_phone) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_PHONE,
    payload: signup_phone,
  });
};
export const setSignupPass = (signup_pass) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_PASS,
    payload: signup_pass,
  });
};
export const setSignupStateName = (signup_state_name) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_STATE_NAME,
    payload: signup_state_name,
  });
};

// **************************** login ******************************//
export const setLoginEmail = (login_email) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_EMAIL,
    payload: login_email,
  });
};
export const setLoginPass = (login_pass) => (dispatch) => {

  dispatch({
    type: SET_LOGIN_PASS,
    payload: login_pass,
  });
};

export const setIsLoginDisabled = (login_disabled) => (dispatch) => {
  dispatch({
    type: IS_LOGIN_DISABLED,
    payload: login_disabled,
  });
};
export const setIsValidEmail = (valid_email) => (dispatch) => {
  dispatch({
    type: IS_VALID_EMAIL,
    payload: valid_email,
  });
};

export const setIsChangeText = (is_change_text) => (dispatch) => {
  dispatch({
    type: IS_CHANGE_TEXT,
    payload: is_change_text,
  });
};

export const setProfile = (set_profile) => (dispatch) => {
  dispatch({
    type: SET_PROFILE,
    payload: set_profile,
  });
};




export const hitUpdateProfileImageApi = (param) => {
  return async (dispatch) => {
    // dispatch({
    //   type: SET_LOADING,
    //   payload: true,
    // });

    // debugLog(param)

    let data = await fetch(`https://demoapps.in/workout/index.php/ImageUpload_controller/update_profile_image`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'multipart/form-data',
      },
      body: param,
    });
    // dispatch({
    //   type: SET_LOADING,
    //   payload: false,
    // });
    let response = await data.json();

    return response;
  };
};

export const setDeleteRow = (id) => (dispatch) => {

  let state = store.getState().indexReducer;
  let user_post = state.user_post;
  console.log(id);
  user_post = user_post.filter((item) =>
    item.id !== id
  );

  dispatch({
    type: DELETE_ROW,
    payload: user_post
  })

}


export const setItemsToArray = (name,hashTag) => (dispatch) => {
  let state = store.getState().indexReducer;
  let empty_data = {  name:name, hashTag:hashTag };
  let data_row = state.user_post;
  
  let neData = [ empty_data,  ...data_row];
  // data_row.push({
  //   name: name,
  //   id: id,
  // });

  // console.log(data_row);
  dispatch({
    type: SET_ITEMS_TO_ARRAY,
    payload: neData,
  });
};

export const setID = (value) => (dispatch) => {
  // console.log('hi');
  dispatch({
    type: ON_ID,
    payload: value,
  });
};

export const setName = (value) => (dispatch) => {
  dispatch({
    type: ON_NAME,
    payload: value,

  });
};