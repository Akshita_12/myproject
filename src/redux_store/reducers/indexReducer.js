import { DELETE_ROW, IS_AUTOCOMPLETE, IS_CHANGE_TEXT, IS_LIKE, IS_LOGIN_DISABLED, IS_VALID_EMAIL, ON_ID, ON_NAME, SET_ITEMS_TO_ARRAY, SET_LOGIN_EMAIL, SET_LOGIN_PASS, SET_NAME, SET_PROFILE, SET_SEARCH, SET_SEARCH_STATE, SET_SIGNUP_EMAIL, SET_SIGNUP_NAME, SET_SIGNUP_PASS, SET_SIGNUP_PHONE, SET_SIGNUP_STATE_NAME, SET_STATE, SET_TEMP, SET_USER_DETAILS, SET_USER_IMAGE } from '../actions/types';

const initialState = {

  // list_data: [],
  // isLoading: true,
  contact_list: [
    {
      name: 'ankita',
    },
    {
      name: 'shiv',
    },
    {
      name: 'ankit',
    },
    {
      name: 'jai',
    },
    {
      name: 'deep',
    },
    {
      name: 'anjali',
    },
    {
      name: 'kajal',
    },
    {
      name: 'kavita',
    }
  ],
  temp: [],
  // autocomplete var
  state_list: [

    {
      name: 'Kerala'
    },
    {
      name: 'Tamilnadu'
    },
    {
      name: 'Punjab'
    },
    {
      name: 'Uttarakhand'
    },
    {
      name: 'Sikkim'
    },
    {
      name: 'Bangalore'
    },
    {
      name: 'Goa'
    },
    {
      name: 'Bihar'
    },
    {
      name: 'Meghalaya'
    },
    {
      name: 'Rajasthan'
    },

  ],

  states: '',
  is_autocomplete: false,

  temp: [],


  user_post: [
    {
      pic: require('../../assets/minions.jpeg'),
      name: 'Akshita',
      id: 'akshi_21',
      likes: '1k.likes',
      message: 'hiii this is akshi',
      // hashTag: '#india',
      hashTag: 'open url'
    },
    {
      pic: require('../../assets/minis.jpeg'),
      name: 'Akshita',
      id: 'akshi_122',
      likes: '1k.likes',
      message: 'hiii this is akshi',
      hashTag: 'https://boxprotraining.co.uk/index.php/terms-conditions/',
    },
    {
      pic: require('../../assets/minions2.jpg'),
      name: 'Shweta',
      id: 'shweta_30',
      likes: '1k.likes',
      message: 'hiii this is akshi',
      hashTag: '#india',
    },

    {
      pic: require('../../assets/mini.jpeg'),
      name: 'Akshita',
      id: 'akshi_12',
      likes: '1.9k likes',
      message: 'hiii this is akshi',
      hashTag: '#india',
    },
    {
      pic: require('../../assets/minions2.jpg'),
      name: 'Kartik',
      id: 'kartik_32',
      likes: '1.9k likes',
      message: 'hiii this is akshi',
      hashTag: '#india',
    },
    {
      pic: require('../../assets/minions.jpeg'),
      name: 'Kriti Sanon',
      id: 'kriti_12',
      likes: '1.9k likes',
      message: 'welcome to react-native',
      hashTag: '#india',
    },
    {
      pic: require('../../assets/minis.jpeg'),
      name: 'Kriti Sanon',
      id: 'kriti_124',
      likes: '1.9k likes',
      message: 'welcome to react-native',
      hashTag: '#india',
    },
  ],

  set_user_image: '',
  set_user_details: '',
  is_like: 0,

  // **************************************************************
  // SIGNUP

  signup_name: '',
  signup_email: '',
  signup_phone: '',
  signup_pass: '',
  signup_state_name: '',

  // **************************************************************
  // LOGIN
  login_email: "",
  login_pass: '',
  isLoginDisabled: true,
  is_valid_email: false,

  is_change_text: false,
  set_profile: '',



  // data_row: [
  //   {
  //     id: "1",
  //     name: 'aaa',
  //   },
  //   {
  //     id: "2",
  //     name: 'ram',
  //   },
  //   {
  //     id: "3",
  //     name: 'kkk',
  //   },
  // ],
  getName: '',
  getId: "",
  
};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_SEARCH:
      return {
        ...state,
        contact_list: action.payload,
      };

    case SET_TEMP:
      return {
        ...state,
        temp: action.payload,
      };


    case IS_AUTOCOMPLETE:
      return {
        ...state,
        is_autocomplete: action.payload,
      };
    case SET_SEARCH_STATE:
      return {
        ...state,
        state_list: action.payload,
      };

    case SET_STATE:
      return {
        ...state,
        states: action.payload,
      };

    case SET_USER_IMAGE:
      return {
        ...state,
        set_user_image: action.payload,
      };
    case SET_USER_DETAILS:
      return {
        ...state,
        set_user_details: action.payload,
      };

    case IS_LIKE:
      return {
        ...state,
        is_like: action.payload,
      };
    // **************************** signup cases******************************//
    case SET_SIGNUP_NAME:
      return {
        ...state,
        signup_name: action.payload,
      };
    case SET_SIGNUP_EMAIL:
      return {
        ...state,
        signup_email: action.payload,
      };
    case SET_SIGNUP_PHONE:
      return {
        ...state,
        signup_phone: action.payload,
      };
    case SET_SIGNUP_PASS:
      return {
        ...state,
        signup_pass: action.payload,
      };
    case SET_SIGNUP_STATE_NAME:
      return {
        ...state,
        signup_state_name: action.payload,
      };

    // **************************** login cases******************************//
    case SET_LOGIN_EMAIL:
      return {
        ...state,
        login_email: action.payload,
      };
    case SET_LOGIN_PASS:
      return {
        ...state,
        login_pass: action.payload,
      };
    case IS_LOGIN_DISABLED:
      return {
        ...state,
        isLoginDisabled: action.payload,
      };
    case IS_VALID_EMAIL:
      return {
        ...state,
        is_valid_email: action.payload,
      };
    case IS_CHANGE_TEXT:
      return {
        ...state,
        is_change_text: action.payload,
      };
    case SET_PROFILE:
      return {
        ...state,
        set_profile: action.payload,
      };

      case DELETE_ROW:
        return {
          ...state,
          user_post: action.payload,
        };

        case ON_NAME:
          return {
            ...state,
            getName: action.payload,
          };
    
          case ON_ID:
          return {
            ...state,
            getId: action.payload,
          };
    
        case SET_ITEMS_TO_ARRAY:
          return {
            ...state,
            user_post: action.payload,
          };
    default:
      return state;
  }
}