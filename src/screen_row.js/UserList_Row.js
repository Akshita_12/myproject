import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';

class UserList_Row extends Component {

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={{
                    backgroundColor: '#80cbc4',
                    elevation: 10,
                    alignItems:'flex-start',
                    paddingHorizontal:10,
                    paddingVertical:10
                }}
                    onPress={() => {
                        this.props.onClick(this.props.dataRow)
                    }}
                >
                    <Image
                        style={{ height: 70, width: 70, borderRadius: 100,alignSelf:'center' }}
                        source={this.props.dataRow.pic}
                    />
                    <Text style={styles.txtStyle} >{this.props.dataRow.name}</Text>
                    <Text style={styles.txtStyle} >{this.props.dataRow.id}</Text>
                    <Text style={styles.txtStyle} >{this.props.dataRow.likes}</Text>
                    <Text style={styles.txtStyle} >{this.props.dataRow.message}</Text>
                    <Text style={styles.txtStyle} >{this.props.dataRow.hashTag}</Text>

                </TouchableOpacity>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    //   txtStyle:{
    //     fontSize:40,
    //     color:'blue',
    //     fontFamily:'serif'
    //   }
});
export default UserList_Row;
