import { useLinkProps } from '@react-navigation/native';
import React from 'react';
import { TouchableOpacity, Text, Image, StyleSheet, View } from 'react-native';

const FlatlistScreen_Row = (props) => {
    return (

        <View style={styles.container}>

            <TouchableOpacity
                onPress={() => { props.onPress(props.dataItem) }}
                style={{
                    backgroundColor: '#80cbc4',
                    elevation: 10,
                    // alignItems:'flex-start',
                    paddingHorizontal: 10,
                    paddingVertical: 10
                }}
            >
                <TouchableOpacity
                    onPress={() => props.onDelete(props.dataItem.id)}
                    style={{ alignItems: 'flex-end' }}>
                    <Image
                        style={{ height: 22, width: 22, }}
                        source={require('../assets/wrong.png')}
                    />
                </TouchableOpacity>
                <Text>
                    {props.dataItem.name}
                </Text>
                <Image
                    style={{ height: 90, width: 90, borderRadius: 100, alignSelf: 'center' }}
                    source={props.dataItem.pic}
                />
                <Text>
                    {props.dataItem.hashTag}
                </Text>
                <Text>
                    {props.dataItem.message}

                </Text>

            </TouchableOpacity>

         
        </View>

        
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    //   txtStyle:{
    //     fontSize:40,
    //     color:'blue',
    //     fontFamily:'serif'
    //   }
});
export default FlatlistScreen_Row;